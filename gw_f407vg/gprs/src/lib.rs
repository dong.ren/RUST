
#![no_std]   //不使用rust标准库
#[macro_use] //添加对打印的宏支持
extern crate std;

use std::*;
use std::fs::File;    
use std::time::Duration;
use std::string::String;
use std::io::Read;
use std::io::Write;


pub const DEV_NAME: &'static str = "/dev/ttyS1";
pub const SERVER_IP: &'static str = "139.224.35.75";
pub const SERVER_PORT: u32 = 4445;


pub struct GPRS{
    pub fd:File,
}
impl GPRS{
    pub fn new(dev_path: &str) -> io::Result<GPRS> {        
        let file = try!(File::open(dev_path));
        Ok(GPRS { fd: file })
    }
    pub fn check_write(&mut self,buf:&[u8]) ->io::Result<(usize)>{
             self.fd.write(buf)
    }
    pub fn check_read(&mut self) -> i8{
        let mut count = 0;
        let mut respond = vec![0u8;500];
        loop{
            let ret = self.fd.read(&mut respond);               
            match ret {
                Ok(ok) => unsafe {
                    let buf = String::from_raw_parts(respond.as_mut_ptr(), ok, ok);        //unsafe {fn from_raw_parts(buf:*mut u8,length:usize,capacity:usize) -> String} 
                    print!("{}",buf);           //at的回复
                    if buf.contains("ERROR") {
                        let res = buf.clone();
                        mem::forget(buf);
                        loop{
                        if res.contains("already opened"){
                            println!("OK");
                            mem::forget(res);
                            break;
                        }
                        if res.contains("+CIPERROR"){
                            println!("send data error");
                            println!("ERROR");
                            mem::forget(res);
                            break;
                        }
                        if res.contains("+CIPOPEN"){
                            println!("OK");
                            mem::forget(res);
                            break;
                        }          
                        count += 1;
                        if count >=5{
                            println!("please restart!!");
                            return 0;
                        }
                        continue;
                        }
                        return 0;
                    }
                    if buf.contains("OK"){
                        println!("OK");
                        mem::forget(buf);
                        print!("\n");
                        return 0;
                    }
                    if buf.contains(">"){
                        mem::forget(buf);
                        return 0;
                    }
                    continue;
                },
                Err(err) => {
                    count = count + 1;
                    if count >= 5 {
                        println!("check_read exec failed, err = {}", err);
                    }
                }
            
                

            }
        }   
    }
    pub fn gprs_init(&mut self){ 
        println!("Module connectivity check");
        self.check_write(b"AT\r\n").unwrap();                    
        thread::sleep(Duration::from_millis(100));
        let ret = self.check_read();
        if ret < 0{
            return;
        }
        
        self.check_write(b"ATE\r\n").unwrap();                    //ATE0关闭回显，ATE打开回显
        thread::sleep(Duration::from_millis(100));
        self.check_read();

        println!("check sim ready");
        self.check_write(b"AT+CPIN?\r\n").unwrap();               //check sim or ready
        thread::sleep(Duration::from_millis(100));
        self.check_read();
       
        println!("signal quality");
        self.check_write(b"AT+CSQ\r\n").unwrap();                  //query signal quality
        thread::sleep(Duration::from_millis(100)); 
        self.check_read();
       
        println!("registration network");
        self.check_write(b"AT+CREG?\r\n").unwrap();                 //network registration
        thread::sleep(Duration::from_millis(100));
        self.check_read();
        
        println!("ip head");
        self.check_write(b"AT+CIPHEAD=0\r\n").unwrap();                 //  +IPD50
        thread::sleep(Duration::from_millis(100));
        self.check_read();
        // self.check_write(b"AT+CGCLASS=B\r\n");         //gprs mode station class
        // thread::sleep(Duration::from_secs(2));
        println!("attached gprs"); 
        self.check_write(b"AT+CGATT=1\r\n").unwrap();             //gprs packet domain attach
        thread::sleep(Duration::from_millis(100));     
        self.check_read();
        
        println!("enable gprs"); 
        self.check_write(b"AT+CGACT=1,1\r\n").unwrap();           //enable gprs         
        thread::sleep(Duration::from_millis(100));   
        self.check_read();
      
        // //开机第一次时不启动，后面每次都启动一次   
        // self.check_write(b"AT+NETCLOSE\r\n");          //close packet network
        // thread::sleep(Duration::from_secs(1));
        // self.check_read();
        // println!("close network");

        
           
    }
    pub fn tcp_connect(&mut self,server_ip:&str,server_port:u32){
        println!("open network");
        self.check_write(b"AT+NETOPEN\r\n").unwrap();            //open packet network
        thread::sleep(Duration::from_millis(100));
        self.check_read();
        
        println!("establish tcp connection");
        let buf = format!("AT+CIPOPEN=1,\"TCP\",\"{}\",{}\r\n",
                          server_ip,
                          server_port);
        self.check_write(&(buf.into_bytes())).unwrap();
        thread::sleep(Duration::from_millis(100));
        self.check_read();
    }
    pub fn tcp_send(&mut self,length:usize){
            let buf = format!("AT+CIPSEND=1,{}\r\n",
                                length);
            self.check_write(&(buf.into_bytes())).unwrap();
            thread::sleep(Duration::from_millis(100));
            self.check_read();
    }
  
}



pub fn tcp_gprs(){
    let mut gp: ::GPRS = GPRS::new(DEV_NAME).unwrap();
    gp.gprs_init();
    gp.tcp_connect(SERVER_IP,SERVER_PORT);
}



