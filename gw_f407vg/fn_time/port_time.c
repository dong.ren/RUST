#include <time.h>
#include<string.h>
#include<stdio.h>

typedef struct port_tm{
    int tm_sec;
    int tm_min;
    int tm_hour;

    int tm_mday;
    int tm_mon;
    int tm_year;

    int tm_wday;
    int tm_yday;
    int tm_isdst;
}port_tm_t;


int port_time_gettime() {
  time_t raw;
  time(&raw);
  return raw;
}

void port_time_settime(int time) {
  struct timespec spec;
  spec.tv_sec = time;
  spec.tv_nsec = 0;
  clock_settime(CLOCK_REALTIME, &spec);
}

// void port_get_time(port_tm_t *local_time){
//   time_t t;
//   struct tm *lt;
//   time(&t);
//   lt = localtime(&t);
//   memcpy(local_time,lt,sizeof(port_tm_t));
//   // printf("%ld\n",local_time->tm_hour);
// }


int port_get_year()
{
  time_t t;
  struct tm *lt;
  time(&t);
  lt = localtime(&t);
  return lt->tm_year+1900;
}

int port_get_month()
{
  time_t t;
  struct tm *lt;
  time(&t);
  lt = localtime(&t);
  return lt->tm_mon;
}

int port_get_day()
{
  time_t t;
  struct tm *lt;
  time(&t);
  lt = localtime(&t);
  return lt->tm_mday;
}





