
#![no_std]
extern crate std;
use std::*;
// #[derive(Debug)]
// pub struct Time{
//     pub tm_sec:i32,
//     pub tm_min:i32,
//     pub tm_hour:i32,
//     pub tm_mday:i32,
//     pub tm_mon:i32,
//     pub tm_year:i32,
//     pub tm_wday:i32,
//     pub tm_yday:i32,
//     pub tm_isdst:i32,
// }
extern "C" {
    pub fn port_time_gettime() -> i32;
    pub fn port_time_settime(time: i32);
    pub fn port_get_year( ) -> i32;
    pub fn port_get_month( ) -> i32;
    pub fn port_get_day( ) -> i32; 
}

// impl Time{
//     pub fn new() -> Time{
//         Time{
//             tm_sec:0,
//             tm_min:0,
//             tm_hour:0,
//             tm_mday:0,
//             tm_mon:0,
//             tm_year:0,
//             tm_wday:0,
//             tm_yday:0,
//             tm_isdst:0,
//         }
//     }
//     pub fn set_time(&mut self,time: i32) {
//         unsafe {
//             port_time_settime(time);
//         }
//     }

//     pub fn get_time(&mut self) -> i32 {
//         unsafe { port_time_gettime() }
//     }

//     // pub fn get_local_time(&mut self,time:*mut Time) {
//     //     unsafe{ port_get_time(time) }
//     // }
//     pub fn get_local_year(&mut self) -> i32{
//         unsafe {port_get_year() }
//     }
//     pub fn get_local_mon(&mut self) -> i32{
//         unsafe {port_get_month() }
//     }
//     pub fn get_local_day(&mut self) -> i32{
//         unsafe {port_get_day() }
//     }
// }


    pub fn set_time(time: i32) {
        unsafe {
            port_time_settime(time);
        }
    }

    pub fn get_time() -> i32 {
        unsafe { port_time_gettime() }
    }

    // pub fn get_local_time(&mut self,time:*mut Time) {
    //     unsafe{ port_get_time(time) }
    // }
    pub fn get_local_year() -> i32{
        unsafe {port_get_year() }
    }
    pub fn get_local_mon() -> i32{
        unsafe {port_get_month() }
    }
    pub fn get_local_day() -> i32{
        unsafe {port_get_day() }
    }








