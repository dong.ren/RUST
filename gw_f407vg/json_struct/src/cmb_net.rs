// extern crate std;
// extern crate gprs;
// extern crate modbus;
// extern crate gw_pvp;
// extern crate fn_flash;
// extern crate fn_time;
// extern crate core;


// use std::*;
// use std::io::Write;
// use std::io::Read;
// use std::thread;
// use std::string::String;
// use std::str;
// use std::vec::Vec;
// // use std::sync::Arc;
// // use std::sync::mutex::Mutex;


// pub const DEV_NAME: &'static str = "/dev/ttyS1";



// /*
// this function should setting dev_id and sta_id
// */



// pub fn cmb_net_begin() -> i8{

//     let dev_id:&str = "1064852484630";
//     // let sta_id:i32 = 1;
//     let mut request_mb_info: ::struct_json::RequestModbusConfInfo =
//                 ::struct_json::RequestModbusConfInfo::new();
//     let mut respond_mb_info: ::struct_json::ResponseModbusConfInfo = 
//                 ::struct_json::ResponseModbusConfInfo::new();
//     let mut report_mb_info: ::struct_json::ReportModbusConfInfo = 
//                 ::struct_json::ReportModbusConfInfo::new();

//     let mut gp:gprs::GPRS = gprs::GPRS::new(DEV_NAME).unwrap();

//     let mut report_time: ::struct_json::RequestTimeInfo = 
//                 ::struct_json::RequestTimeInfo::new();
//     let mut respond_time_info: ::struct_json::ResponseTimeInfo = 
//                 ::struct_json::ResponseTimeInfo::new();

//     let mut report_battery_info: ::struct_json::ReportBatteryInfo = 
//                 ::struct_json::ReportBatteryInfo::new();


//     let mut respond_set_warning = ::struct_json::ResponseSetWarning::new(); 

//     //from flash getting current,voltage,temp,eletric_arc 
//     //storage it in report_time 
//     get_warning_from_flash(&mut report_time);
//     //in order to sync time and get sta_id
//     let mut sta_id:i32 = 0;
//     let mut time_stamp:i32 = 0;
//     parse_time_data(&mut report_time,&mut respond_time_info,&mut gp,&mut sta_id,&mut time_stamp);
//     println!("start time is：{}",time_stamp);


//     //parse mb to getting mb return's data
//     parse_mb_data(&mut request_mb_info,&mut respond_mb_info,&mut gp,dev_id,sta_id);





//     //impl pvp
//     let mut pvp = gw_pvp::CbmPvp::new();
//     pvp.pvp_start();
//     pvp.get_pvp();
//     let check_24_v = pvp.check_24_v();





//     //mb的实现
//     let mut cmd_mb = modbus::ModBus::new("/dev/ttyS4").unwrap();
//         // buf是共享资源，对其进行保护
//         // let mut buf = Arc::new(Mutex::new(vec![0u8;50]));
//     // let mut buf = vec![0u8;50];
//     // println!("mb start");


//     //103 030 com_hardware eletric_arc handle
//     report_time_data_handle(&mut cmd_mb,&mut report_time);
//     report_time.set_dev_id(dev_id);
    
//     /*
//     upload check eletric_arc value but we need to setting eletric_arc 
//     so we need to compare eletric_arc 
//     */
    

//     let mut current = [0u16;10];
//     //getting channel current and hold in array current
//     getting_channel_current(&mut cmd_mb,&mut respond_mb_info,&mut current);
    




//     //getting each battery info 
//     //include addr,voltage,power,energy
//     report_battery_info.set_dev_id(dev_id);
//     report_battery_info.set_sta_id(response_sta_id);
//     report_mb_info.set_report_dev_id(dev_id);     
//     report_mb_info.set_report_sta_id(response_sta_id);
//     getting_each_battery_info(&mut cmd_mb,&mut report_battery_info,&mut respond_mb_info,&mut report_mb_info,time_stamp,&mut respond_time_info,&mut current,&mut gp);

//         //掉电检测判断
//     if check_24_v != 1{
//         //report warning
//         report_warning(&mut report_time,&mut gp);
//     }else{
//         report_battery_time_mb_data(&mut report_time,&mut gp,&mut report_battery_info,&mut report_mb_info);
//     }
//         loop{}
    
// } 
// pub fn parse_mb_data(request_mb_info:&mut ::struct_json::RequestModbusConfInfo,
//                     respond_mb_info:&mut ::struct_json::ResponseModbusConfInfo,
//                     gp:&mut gprs::GPRS,
//                     dev_id:&str,
//                     sta_id:i32){

//     request_mb_info.set_request_dev_id(dev_id);
//     println!("dev_id is {}",dev_id);
//     request_mb_info.set_request_sta_id(sta_id);
//     println!("sta_id is {}",sta_id);

//     let send_request_len = request_mb_info.struct_to_json().len();
//     gp.tcp_send(send_request_len);
//     let mut recv_from_the_server = vec![0u8;250];
//     gp.fd.write(&(request_mb_info.struct_to_json().as_bytes())).unwrap();
//     println!("request mb info send success!");

//     thread::sleep(std::time::Duration::from_millis(500));
//     let res_recv = gp.fd.read(&mut recv_from_the_server);
//     let str_recv:usize;
//     match res_recv {
//         Ok(ok) => {
//             str_recv = ok;
//         }
//             Err(_) => {
//                 print!("can not understand the server info!\n");
//                 return;
//             }
//         }
//     let mut res = String::new();
//     unsafe{
//         let buf1 = String::from_raw_parts(recv_from_the_server.as_mut_ptr(),str_recv,str_recv); 
//             // println!("{}",buf1);
//         if buf1.contains("\n"){
//             let _temp_str = buf1.clone();
//             let v:Vec<&str> = _temp_str.splitn(2,'\n').collect();
//             res.push_str(&v[1]);
//             //去掉返回回来的字节数
//             print!("{}",res);
//             if res.contains("pkg_code"){ 
//                 println!("return mb info recv success!");
//             }
//         }
//     }
//     let x = respond_mb_info.respond_json_to_mb_struct(&res);
//     if x < 0{
//         println!("parse error");
//         return;
//     }

// }

pub fn parse_time_data(report_time:&mut ::struct_json::RequestTimeInfo,
                            respond_time_info:&mut ::struct_json::ResponseTimeInfo,
                            gp:&mut gprs::GPRS,sta_id: &mut i32,time_stamp:&mut i32){
    //report time 
    // report_time.set_com_hard(0xFFFF);
    let send_report_time_len = report_time.struct_to_json().len();
    gp.tcp_send(send_report_time_len);
    let mut recv_from_the_server = vec![0u8;250];

    gp.fd.write(&(report_time.struct_to_json().as_bytes())).unwrap();
    thread::sleep(std::time::Duration::from_millis(100));
    println!("report time success !");

    let time_recv = gp.fd.read(&mut recv_from_the_server);
    let recv_time:usize;
    match time_recv {
        Ok(ok) => {
            recv_time = ok;
        }
        Err(_) => {
            print!("can not understand the server info!\n");
            return;
        }
    }
    let mut res_time = String::new();
    unsafe{
    let buf1 = String::from_raw_parts(recv_from_the_server.as_mut_ptr(),recv_time,recv_time); 
                // println!("{}",buf1);
    if buf1.contains("\n"){
        let _temp_str = buf1.clone();
        let v:Vec<&str> = _temp_str.splitn(2,'\n').collect();
        res_time.push_str(&v[1]);
                //去掉读取的字节数,只打印返回
        print!("{}",res_time);
        if res_time.contains("pkg_code"){ 
            println!("return time info recv success !");
            }
        }
    }
            //解析返回时间数据
    let x = respond_time_info.json_to_struct(&res_time);     
    if x < 0{
        println!("parse error");
        return ;
    }

    *sta_id = respond_time_info.get_sta_id();
    *time_stamp = respond_time_info.get_timestamp();
    fn_time::set_time(respond_time_info.get_timestamp());
    
}

// pub fn report_time_data_handle(cmd_mb: &mut modbus::ModBus,report_time:&mut ::struct_json::RequestTimeInfo){
//     let mb_slave_addr = 247;

//     //030-103 data handle
//     let mut buf = vec![0u8;50];
//     let reg_length_info_hall_1 = 0x4;
//     let reg_addr_info_hall_1 = 0x0020;
//     cmd_mb.read_holding_reg(mb_slave_addr, reg_addr_info_hall_1, reg_length_info_hall_1, &mut buf, 2);
//     let f030_soft_version = (buf[0] as u16) << 8 | buf[1] as u16;
//     report_time.set_030_soft(f030_soft_version);
//     let f030_hard_version = (buf[2] as u16) << 8 | buf[3] as u16;
//     report_time.set_030_hard(f030_hard_version);
//     let f103_hard_version = f030_hard_version;
//     report_time.set_103_hard(f103_hard_version);
//     let f103_soft_version = (buf[6] as u16) << 8 | buf[7] as u16;
//     report_time.set_103_soft(f103_soft_version);
//     println!("f030_soft_version = {}", f030_soft_version);
//     println!("f030_hard_version = {}", f030_hard_version);
//     println!("f103_soft_version = {}", f103_soft_version);
//     println!("f103_hard_version = {}", f103_hard_version);

//     //com_hardware_handle
//     buf.clear();
//     let mut buf = vec![0u8;50];
//     let reg_length_info_hall_2 = 0x1;
//     let reg_addr_info_hall_2 = 0xFF80;
//     cmd_mb.read_holding_reg(mb_slave_addr, reg_addr_info_hall_2, reg_length_info_hall_2, &mut buf, 2);
//     let com_hardware = (buf[0] as u16) << 8 | buf[1] as u16;
//     report_time.set_com_hard(com_hardware);
//     println!("com_hardware is = {}", com_hardware);

//     //eletric_arc handle
//     buf.clear();
//     let mut buf = vec![0u8;50];
//     let reg_length_info_hall_3 = 0x1;
//     let reg_addr_info_hall_3 = 0x000B;           
//     cmd_mb.read_holding_reg(mb_slave_addr, reg_addr_info_hall_3, reg_length_info_hall_3, &mut buf, 2);
//     let lose_current = (buf[0] as u16) << 8 | buf[1] as u16;
//     report_time.set_eletric_arc(lose_current);
//     println!("lose current = {}",lose_current);
// }
// //getting channel current and hold in array current
// pub fn getting_channel_current(cmd_mb: &mut modbus::ModBus,respond_mb_info: &mut ::struct_json::ResponseModbusConfInfo,
//                                      current:&mut [u16;10]){
//         let mut buf = vec![0u8;50];
//         let mb_slave_addr = 247;
//         let mb_reg_start_addr_current = 0x0;
//         let mb_reg_data_num_current = 0x5;    
//         cmd_mb.read_holding_reg(mb_slave_addr, mb_reg_start_addr_current, mb_reg_data_num_current, &mut buf, 2);
//         for i in 0..respond_mb_info.get_channel_nums(){
//             current[i as usize] = ((buf[2 * i as usize]) as u16) << 8 | (buf[1 + 2 * i as usize]) as u16;
//         }
// }


// /*
// *一直循环 大约有多少电池板 读到多少不同地址就上报 相同就不上报 没有读到的就是没在线的 
// *每个通道都要这样读 统计每个通道没在线的显示到屏幕
// *把所有从modbus读出的buf[0]放在一个新建数组中
// */
// pub fn getting_each_battery_info(cmd_mb:&mut modbus::ModBus,
//                                     report_battery_info:&mut::struct_json::ReportBatteryInfo,
//                                     respond_mb_info:&mut ::struct_json::ResponseModbusConfInfo,
//                                     report_mb_info:&mut ::struct_json::ReportModbusConfInfo,
//                                     time_stamp:i32,
//                                     respond_time_info:&mut ::struct_json::ResponseTimeInfo,
//                                     current:&mut [u16;10],
//                                     gp:&mut gprs::GPRS,){

//         let mut buf = vec![0u8;50];
//         let mb_slave_addr = 247;
//         let mb_reg_start_addr_battery = 0x5000;
//         let mb_reg_data_num_battery = 0x9;
        
//         report_battery_info.set_netdev_code(0);
//         report_battery_info.set_sec(fn_time::get_time());
//         report_battery_info.set_cluster_count(respond_mb_info.get_channel_nums() as i8);      
//         report_battery_info.set_battery_nums(respond_mb_info.get_battery_nums(0) as i8);      
              
//         // report_mb_info.set_report_sec(fn_time::get_time());      
//         // report_mb_info.set_seg_nums(mb_reg_data_num_battery);        
//         for i in 0..respond_mb_info.get_channel_nums(){        
//             for j in 0..respond_mb_info.get_battery_nums(i) * 2{
//                 buf.clear();
//                 let mut buf = vec![0u8;50];
//                 cmd_mb.read_holding_reg(mb_slave_addr, (mb_reg_start_addr_battery + (9 * i)) as u16, mb_reg_data_num_battery as u16, &mut buf, 2);
//                 let mut time_for_energy = fn_time::get_time() - time_stamp;
//                 battery_data_handle(report_battery_info,& time_for_energy,&mut buf,i,current);
//                 // report_mb_data_handle(report_mb_info,&mut buf);
//             }
//             //write data to report_battery_data.count
//             report_battery_info.check_online();
//             //report battery_info and checck each string current,voltage,temp also report fault info
//             battery_warning_hadle(report_battery_info,i,gp);
            
//         }
// }

// pub fn battery_data_handle(report_battery_info:&mut ::struct_json::ReportBatteryInfo,time_for_energy:& i32,
//                                             buf: &mut Vec<u8>,i:i32,current:&mut[u16;10]){
//     if buf[1] != 0{                                         
//         let battery_addr:i32 = buf[0] as i32;                                                          
//         report_battery_info.set_addr_a((battery_addr+1) as i8,battery_addr);
//         let voltage = (buf[2] as i32) << 8 | buf[3] as i32;   
//         report_battery_info.set_voltage_a(voltage,battery_addr);
        
//         report_battery_info.set_cluster_index((i + 1) as i8);         
//         let power = voltage * current[i as usize] as i32;          
//         report_battery_info.set_power_a(power as i32,battery_addr);
//         report_battery_info.set_current_a(current[i as usize] as i16,battery_addr);
        
        
//         //energy
//         let sum_energy = power * time_for_energy;
//         report_battery_info.set_energy_a(sum_energy,battery_addr);

        
//         let noise = (buf[8] as u16) << 8 | buf[9] as u16;
//         let signal = (buf[10] as u16) << 8 | buf[11] as u16;
//         let noise_signal:i32 = (noise as i32) << 16| signal as i32;       //高16位为噪声，低为信号
//         report_battery_info.set_signal_a(noise_signal,battery_addr);
        
//         let sw_version = (buf[6] as u16) << 8 | buf[7] as u16;
//         //buf[5] is battery status
//         report_battery_info.set_status_a((sw_version as i32) << 16 | buf[5] as i32 ,battery_addr);
//         //buf[4] is battery_temp
//         let hw_version = (buf[12] as u16) << 8 | buf[13] as u16;
//         report_battery_info.set_temp_a((hw_version as i32) << 16 | buf[4] as i32,battery_addr);
//         // let mut battery_sn_high = (buf[14] as u32) << 8 | buf[15] as u32;
//         // let mut battery_sn_low = (buf[16] as u32) << 8 | buf[17] as u32;

//         // report_battery_info.struct_to_json();
//     }else{
//         println!("modbus data error!");
//     }

// }
// /*
// battery_warning_hadle 处理电弧报警信息
// */
// pub fn battery_warning_hadle(report_battery_info:&mut ::struct_json::ReportBatteryInfo,
//                                 i:i32,gp: &mut gprs::GPRS,){
//     let mut temp_warn_opt = report_battery_info.check_no_current_warning(i);
//     if temp_warn_opt != "" {
//         let send_no_current_warning_len = temp_warn_opt.len();
//         gp.tcp_send(send_no_current_warning_len);
//         gp.fd.write(&(temp_warn_opt.as_bytes())).unwrap();
//         println!("report_no_current_warning ok!");
//         report_battery_info.set_sec(fn_time::get_time());
//     }
//     temp_warn_opt = report_battery_info.check_low_current_warning(i);
//     if temp_warn_opt != "" {
//         let send_low_current_warning_len = temp_warn_opt.len();
//         gp.tcp_send(send_low_current_warning_len);
//         gp.fd.write(&(temp_warn_opt.as_bytes())).unwrap();
//         println!("report_low_current_warning ok!");
//         report_battery_info.set_sec(fn_time::get_time());
//     }
//     temp_warn_opt = report_battery_info.check_high_current_warning(i);
//     if temp_warn_opt != "" {
//         let send_high_current_warning_len = temp_warn_opt.len();
//         gp.tcp_send(send_high_current_warning_len);
//         gp.fd.write(&(temp_warn_opt.as_bytes())).unwrap();
//         println!("report_high_current_warning ok!");
//         report_battery_info.set_sec(fn_time::get_time());
//     }
//     temp_warn_opt = report_battery_info.check_low_voltage_warning(i);
//     if temp_warn_opt != "" {
//         let send_low_voltage_warning_len = temp_warn_opt.len();
//         gp.tcp_send(send_low_voltage_warning_len);
//         gp.fd.write(&(temp_warn_opt.as_bytes())).unwrap();
//         println!("report_low_voltage_warning ok!");
//         report_battery_info.set_sec(fn_time::get_time());
//     }
//     temp_warn_opt = report_battery_info.check_high_voltage_warning(i);
//     if temp_warn_opt != "" {
//         let send_high_voltage_warning_len = temp_warn_opt.len();
//         gp.tcp_send(send_high_voltage_warning_len);
//         gp.fd.write(&(temp_warn_opt.as_bytes())).unwrap();
//         println!("report_high_voltage_warning ok!");
//         report_battery_info.set_sec(fn_time::get_time());
//     }
//     temp_warn_opt = report_battery_info.check_high_temp_warning(i);
//     if temp_warn_opt != "" {
//         let send_high_temp_warning_len = temp_warn_opt.len();
//         gp.tcp_send(send_high_temp_warning_len);
//         gp.fd.write(&(temp_warn_opt.as_bytes())).unwrap();
//         println!("report_high_temp_warning ok!");
//         report_battery_info.set_sec(fn_time::get_time());
//     }
//     // report_battery_info.check_online();

// }
// pub fn report_mb_data_handle(report_mb_info:&mut ::struct_json::ReportModbusConfInfo,buf: &mut Vec<u8>){
//     for i in 0..4{
//     report_mb_info.set_report_start_addr(0x5000 as i32 + (9 * i) as i32,i as i32);  //0x5000 0x5009 0x5012 0x501b
//     report_mb_info.set_report_length(9 as i8,i as i32); //[9,9,9,9]
//     for m in 0..9{  
//         report_mb_info.set_report_data((buf[m as usize] as i32) << 8 | (buf[m as usize + 1 ])as i32,i as i32);
//         }
//     }
//     report_mb_info.struct_to_json();
// }




// pub fn send_battery_info_to_flash(array_battery: &Vec<i32>) {
//     let mut send_to_the_flash_buf: [u8; 160] = [0; 160];
//     for (i, j) in array_battery.iter().enumerate() {              //返回数组中下标和下标里的值
//         send_to_the_flash_buf[i * 8 + 0] = (j % 0xFF) as u8;
//         send_to_the_flash_buf[i * 8 + 1] = ((j % 0xFFFF) / 0xFF) as u8;
//         send_to_the_flash_buf[i * 8 + 2] = ((j % 0xFFFFFF) / 0xFFFF) as u8;
//         send_to_the_flash_buf[i * 8 + 3] = (j / 0xFFFFFF) as u8;
//         send_to_the_flash_buf[i * 8 + 4] = 0;
//         send_to_the_flash_buf[i * 8 + 5] = 0;
//         send_to_the_flash_buf[i * 8 + 6] = 0;
//         send_to_the_flash_buf[i * 8 + 7] = 0;
//     }
//     let mut flash_opt: fn_flash::flash_fn::FlashOpt;
//     let flash_opt_res = fn_flash::flash_fn::FlashOpt::new(::struct_json::FLASH_NAME);
//     match flash_opt_res {
//         Ok(ok) => flash_opt = ok,
//         Err(_) => return,
//     }
//     //把buf写到指定位置
//     let _to_flash = flash_opt.write_file(&send_to_the_flash_buf,
//                                          fn_flash::flash_fn::FLASH_BATTERY_ENERGY_01 as u64);
//     match _to_flash {
//         Ok(ok) => print!("{:?}\n", ok),
//         Err(_) => print!("{:?}\n", "flash write fails"),
//     }
// }
// pub fn send_warning_to_flash(location: u64, value: i32) {
//     let mut flash_opt: fn_flash::flash_fn::FlashOpt;
//     let flash_opt_res = fn_flash::flash_fn::FlashOpt::new(::struct_json::FLASH_NAME);
//     match flash_opt_res {
//         Ok(ok) => flash_opt = ok,
//         Err(_) => return,
//     }
//     let mut send_to_flash_buf: [u8; 8] = [0; 8];
//     send_to_flash_buf[0] = (value % 0xFF) as u8;
//     send_to_flash_buf[1] = ((value % 0xFFFF) / 0xFF) as u8;
//     send_to_flash_buf[2] = ((value % 0xFFFFFF) / 0xFFFF) as u8;
//     send_to_flash_buf[3] = (value / 0xFFFFFF) as u8;
//     send_to_flash_buf[4] = 0;
//     send_to_flash_buf[5] = 0;
//     send_to_flash_buf[6] = 0;
//     send_to_flash_buf[7] = 0;
//     let _to_flash = flash_opt.write_file(&send_to_flash_buf, location);
//     match _to_flash {
//         Ok(ok) => print!("{:?}\n", ok),
//         Err(_) => print!("{:?}\n", "flash write fails"),
//     }
// }





// /*
// from flash getting sum energy  hold in array sum_energy_temp
// */
// pub fn get_battery_from_flash(sum_energy_temp: &mut Vec<i32>) {
//     let mut flash_opt: fn_flash::flash_fn::FlashOpt;
//     let flash_opt_res = fn_flash::flash_fn::FlashOpt::new(::struct_json::FLASH_NAME);
//     match flash_opt_res {
//         Ok(ok) => flash_opt = ok,
//         Err(_) => return,
//     }             
//     let mut recv_from_flash: [u8; 160] = [0; 160];
//     let len_form_flash;                                 //flash_fn
//     let _res_read = flash_opt.read_file(&mut recv_from_flash,
//                                         fn_flash::flash_fn::FLASH_BATTERY_ENERGY_01 as u64);     
//     match _res_read {
//         Ok(ok) => len_form_flash = ok,
//         Err(_) => return,
//     }
    
//     if len_form_flash == 160 {
//         for i in 0..::struct_json::MAX_BATTERY {              
//             let temp_array = [recv_from_flash[i * 8 + 0],
//                               recv_from_flash[i * 8 + 1],
//                               recv_from_flash[i * 8 + 2],
//                               recv_from_flash[i * 8 + 3],
//                               recv_from_flash[i * 8 + 4],
//                               recv_from_flash[i * 8 + 5],
//                               recv_from_flash[i * 8 + 6],
//                               recv_from_flash[i * 8 + 7]];     //159
//             let t_i64: ::struct_json::TempI64;
//             unsafe {
//                 t_i64 = core::mem::transmute_copy(&temp_array);      
//             }
//             if t_i64.num != -1 {
//                 sum_energy_temp[i] = t_i64.num as i32;  //sum_energy_temp[20]    
//             } else {
//                 sum_energy_temp[i] = 0;
//             }
//         }
//     }
// }

pub fn get_warning_from_flash(report_time: &mut ::struct_json::RequestTimeInfo) {
    let mut flash_opt: fn_flash::flash_fn::FlashOpt;
    let flash_opt_res = fn_flash::flash_fn::FlashOpt::new(::struct_json::FLASH_NAME);
    match flash_opt_res {
        Ok(ok) => flash_opt = ok,
        Err(_) => return,
    }
    let mut recv_from_flash: [u8; 64] = [0; 64];
    let len_form_flash;
    let _res_read = flash_opt.read_file(&mut recv_from_flash,
                   fn_flash::flash_fn::FLASH_BATTERY_WARNING_NO_CURRENT as u64);
    match _res_read {
        Ok(ok) => len_form_flash = ok,//从flash指定位置去读no_current
        Err(_) => return,
    }
    if len_form_flash == 64 {
        let mut warn_info: [i64; 8] = [0; 8];
        for i in 0..8 {
            let temp_array = [recv_from_flash[i * 8 + 0], //0　　//56
                              recv_from_flash[i * 8 + 1],
                              recv_from_flash[i * 8 + 2],
                              recv_from_flash[i * 8 + 3],
                              recv_from_flash[i * 8 + 4],
                              recv_from_flash[i * 8 + 5],
                              recv_from_flash[i * 8 + 6],
                              recv_from_flash[i * 8 + 7]]; //7   //63
            let t_i64: ::struct_json::TempI64;
            unsafe {
                t_i64 = core::mem::transmute_copy(&temp_array);
            }
            warn_info[i] = t_i64.num;        //56到63的数据才有用吗？
        }
        if warn_info[0] != -1 {
            report_time.set_no_current(warn_info[0] as u16);
        }
        if warn_info[1] != -1 {
            report_time.set_low_current(warn_info[1] as u16);
        }
        if warn_info[2] != -1 {
            report_time.set_high_current(warn_info[2] as u16);
        }
        if warn_info[3] != -1 {
            report_time.set_over_current(warn_info[3] as u16);
        }
        if warn_info[4] != -1 {
            report_time.set_low_voltage(warn_info[4] as u16);
        }
        if warn_info[5] != -1 {
            report_time.set_high_voltage(warn_info[5] as u16);
        }
        if warn_info[6] != -1 {
            report_time.set_high_temp(warn_info[6] as u16);
        }
        if warn_info[7] != -1 {
            report_time.set_eletric_arc(warn_info[7] as u16);
        }
    }
}





// pub fn send_and_recv_total_info(gp: &mut gprs::GPRS,
//                                 dev_id: &str,
//                                 sum_energy_temp: &Vec<i32>,
//                                 request_time_info: &mut ::struct_json::RequestTimeInfo,
//                                 respond_time_info: &mut ::struct_json::ResponseTimeInfo,
//                                 sta_id: &mut i32,response_modbus_conf_info: &::struct_json::ResponseModbusConfInfo,
//                                 time_for_energy: &mut i32,sum_energy_before: &mut i64,count_for_save: &mut i8,count_for_time:&mut u8) {
//     *count_for_save = *count_for_save + 1;
//     *count_for_time = *count_for_time + 1;
//     let mut request_total_info = ::struct_json::RequestTotalPower::new();
//     request_total_info.set_dev_id(dev_id);
//     request_total_info.set_sta_id(*sta_id);
//     request_total_info.set_sec(respond_time_info.get_timestamp());
//     let mut total_energy: i64 = 0;
//     for i in 0..response_modbus_conf_info.get_battery_nums(0) {        //0..20
//         total_energy = total_energy + sum_energy_temp[i as usize] as i64;
//     }
//     request_total_info.set_energy((total_energy / 10000) as i32);
//     request_total_info.set_power(((total_energy - *sum_energy_before) * 3600 /
//                                   (respond_time_info.get_timestamp() - *time_for_energy) as i64 /
//                                   10000) as i32);
//     *time_for_energy = respond_time_info.get_timestamp();
//     *sum_energy_before = total_energy;
//     if *count_for_save >= 5 {
//         *count_for_save = 0;
//         send_battery_info_to_flash(&sum_energy_temp);
//     }
    
//     let send_str_temp = request_total_info.struct_to_json();
//     print!("{}\n", send_str_temp);
//     let send_str_temp_len = send_str_temp.len();
//     gp.tcp_send(send_str_temp_len);
//     gp.fd.write(&(send_str_temp.as_bytes())).unwrap();          
//     println!("total info send ok");
//     std::thread::sleep(std::time::Duration::from_millis(100));

//     let mut recv_from_the_server: [u8; 256] = [0; 256];
//     let res_recv = gp.fd.read(&mut recv_from_the_server);
//     let res_str;
//     match res_recv {
//         Ok(ok) => {
//             res_str = ok;
//         }
//         Err(_) => {
//             print!("can not understand the server info!\n");
//             return;
//         }
//     }
//     let mut res = String::new();
//     unsafe{
//         let buf1 = String::from_raw_parts(recv_from_the_server.as_mut_ptr(),res_str,res_str); 
//             // println!("{}",buf1);
//         if buf1.contains("\n"){
//             let _temp_str = buf1.clone();
//             let v:Vec<&str> = _temp_str.splitn(2,'\n').collect();
//             res.push_str(&v[1]);
//             //去掉返回回来的字节数
//             print!("{}",res);
//             if res.contains("pkg_code"){ 
//                 println!("return total info recv success!");
//             }
//         }
//     }

//     let mut response_set_warning = ::struct_json::ResponseSetWarning::new();
//     //上报改造电站逆变器的总信息　文档上没有返回的  实际上返回的是设置报警参数
//     let res_jts = response_set_warning.json_to_struct(&res);
//     if res_jts < 0 {
//         return;
//     }
//     if response_set_warning.check_no_current_warning(request_time_info.get_no_current() as i32) {
//         request_time_info.set_no_current(response_set_warning.get_no_current() as u16);
//         send_warning_to_flash(fn_flash::flash_fn::FLASH_BATTERY_WARNING_NO_CURRENT as u64,
//                                   response_set_warning.get_no_current());
//     }
//     if response_set_warning.check_low_current_warning(request_time_info.get_low_current() as i32) {
//         request_time_info.set_low_current(response_set_warning.get_low_current() as u16);
//         send_warning_to_flash(fn_flash::flash_fn::FLASH_BATTERY_WARNING_LOW_CURRENT as u64,
//                                   response_set_warning.get_low_current());
//     }
//     if 
//         response_set_warning.check_high_current_warning(request_time_info.get_high_current() as i32) {
//         request_time_info.set_high_current(response_set_warning.get_high_current() as u16);
//         send_warning_to_flash(fn_flash::flash_fn::FLASH_BATTERY_WARNING_HIGH_CURRENT as u64,
//                                   response_set_warning.get_high_current());
//     }
//     if 
//         response_set_warning.check_over_current_warning(request_time_info.get_over_current() as i32) {
//         request_time_info.set_over_current(response_set_warning.get_over_current() as u16);
//         send_warning_to_flash(fn_flash::flash_fn::FLASH_BATTERY_WARNING_OVER_CURRENT as u64,
//                                   response_set_warning.get_over_current());
//     }
//     if response_set_warning.check_low_voltage_warning(request_time_info.get_low_voltage() as i32) {
//         request_time_info.set_low_voltage(response_set_warning.get_low_voltage() as u16);
//         send_warning_to_flash(fn_flash::flash_fn::FLASH_BATTERY_WARNING_LOW_VOLTAGE as u64,
//                                   response_set_warning.get_low_voltage());
//     }
//     if 
//         response_set_warning.check_high_voltage_warning(request_time_info.get_high_voltage() as i32) {
//         request_time_info.set_high_voltage(response_set_warning.get_high_voltage() as u16);
//         send_warning_to_flash(fn_flash::flash_fn::FLASH_BATTERY_WARNING_HIGH_VOLTAGE as u64,
//                                   response_set_warning.get_high_voltage());
//     }
//     if response_set_warning.check_high_temp_warning(request_time_info.get_high_temp() as i32) {
//         request_time_info.set_high_temp(response_set_warning.get_high_temp() as u16);
//         send_warning_to_flash(fn_flash::flash_fn::FLASH_BATTERY_WARNING_HIGH_TEMP as u64,
//                                   response_set_warning.get_high_temp());
//     }
//     *count_for_time = 175;
// }















// /*
// report_warning:cheek_24_v为低电平　则上报no_current
// */
// pub fn report_warning(report_time:&mut ::struct_json::RequestTimeInfo,
//                         gp:&mut gprs::GPRS){
//     report_time.set_no_current(0);
//     let send_report_time_len = report_time.struct_to_json().len();
//     gp.tcp_send(send_report_time_len);

//     gp.fd.write(&(report_time.struct_to_json().as_bytes())).unwrap();
//     thread::sleep(std::time::Duration::from_millis(100));
//     println!("report_warning ok!");
// }
// /*
// report_battery_time_mb_data：上报各个结构体中存储的数据
// */
// pub fn report_battery_time_mb_data(report_time:&mut ::struct_json::RequestTimeInfo,
//                                     gp:&mut gprs::GPRS,
//                                     report_battery_info:&mut ::struct_json::ReportBatteryInfo,
//                                     report_mb_info:&mut ::struct_json::ReportModbusConfInfo){
//     let send_report_time_len = report_time.struct_to_json().len();
//     gp.tcp_send(send_report_time_len);
//     gp.fd.write(&(report_time.struct_to_json().as_bytes())).unwrap();
//     thread::sleep(std::time::Duration::from_millis(100));
//     println!("report_time ok!");

//     let send_report_battery_len = report_battery_info.struct_to_json().len();
//     gp.tcp_send(send_report_battery_len);
//     gp.fd.write(&(report_battery_info.struct_to_json().as_bytes())).unwrap();
//     thread::sleep(std::time::Duration::from_millis(100));
//     println!("report_battery ok!");

//     //report mb_data
//     let send_report_mb_len = report_mb_info.struct_to_json().len();
//     gp.tcp_send(send_report_mb_len);
//     gp.fd.write(&(report_mb_info.struct_to_json().as_bytes())).unwrap();
//     thread::sleep(std::time::Duration::from_millis(100));
//     println!("request_respond_report ok!");

// }









