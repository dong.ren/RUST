
// #![no_std]
extern crate std;
extern crate json;
use std::*; //使用引入的包里的模块和部分函数.
use std::prelude::*;
use std::convert::Into;
use std::clone::Clone;
use std::string::String;
#[warn(unused_must_use)]
pub const SW_VERSION: u16 = 0x1;
pub const MAX_BATTERY: usize = 20;
pub const FLASH_NAME: &'static str = "/dev/mtdblock0";

pub struct TempI64 {
    pub num: i64,
}



//设置报警参数
#[derive(Debug)]
pub struct SetWarning {
    pkg_code: i8, //0x06
    no_current: i16, //ma
    low_current: i16,
    high_current: i32,
    over_current: i32,
    low_voltage: i32, //10mv
    high_voltage: i32,
    high_temp: i8, //c
}
impl SetWarning {
    pub fn new() -> SetWarning {
        SetWarning {
            pkg_code: 0x06,
            no_current: 1,
            low_current: 500,    //ma
            high_current: 10000,
            over_current: 50000,
            low_voltage: 2000,
            high_voltage: 100000,
            high_temp: 80,
        }
    }
    //if check_no_current_warning(CHECK_24V) != ture
    pub fn check_no_current_warning(&self, value: i16) -> bool {
        if self.no_current & value != 0 {
            true
        } else {
            false
        }
    }

    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["no_current"] = self.no_current.into();
        data["low_current"] = self.low_current.into();
        data["high_current"] = self.high_current.into();
        data["over_current"] = self.over_current.into();
        data["low_voltage"] = self.low_voltage.into();
        data["high_voltage"] = self.high_voltage.into();
        data["high_temp"] = self.high_temp.into();
        let tem_str = data.dump() + "\r\n";
        print!("{}", tem_str);
        tem_str
    }
    pub fn check_low_current_warning(&self, value: i32) -> bool {
        if self.no_current as i32 != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_current_warning(&self, value: i32) -> bool {
        if self.high_current != value {
            true
        } else {
            false
        }
    }
    pub fn check_over_current_warning(&self, value: i32) -> bool {
        if self.over_current != value {
            true
        } else {
            false
        }
    }
    pub fn check_low_voltage_warning(&self, value: i32) -> bool {
        if self.low_voltage != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_voltage_warning(&self, value: i32) -> bool {
        if self.high_voltage != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_temp_warning(&self, value: i32) -> bool {
        if self.high_temp as i32 != value {
            true
        } else {
            false
        }
    }
}







/*
mb数据信息：
let data = json::parse(r#"
{
    "pkg_code":"0x03",
    "sta_id":0,
    "netdev_code":0,
    "dev_id":0,
    "sec":0,
    "seg_nums":0,
    "seg":{
        "star_addr":[0],
        "length":[0],
        "data":[[0],[0],[0]]
    }
}"#).unwrap();

prinln!("{:?}",data);
data.dump();
data["seg"]["data"][0] = "23";
data["seg"]["data"][1] = "32";
*/

#[derive(Debug)]
//请求modbus配置信息
pub struct RequestModbusConfInfo {
    pkg_code: i8, //0x02
    dev_id: String, //逆变器的id          1064852484630
    sta_id: i32, //0   通信盒所属电站id
}
// //响应
// {
//     "pkg_code":"num",  //0x02
//     "dev_id":"num",
//     "mb_addr":"num",
//     "mb_baud":"num",
//     "mb_fmt":"num",              //"0:8N1,1:8N2,2:8E1,3:801"
//     "channel_count":["num"],     //电池板通道数量
//     "battery_nums":["num"],      //每个通道分别有多少个电池板，元素与‘channel_count’各元素对应
//     "seg":
//     {
//         "seg_num":"num",          //mb寄存器地址段数
//         "start_addr":["num"],
//         "lengh":["num"]
//     }
// }
impl RequestModbusConfInfo {
    pub fn new() -> RequestModbusConfInfo {
        RequestModbusConfInfo {
            pkg_code: 0x02,
            dev_id: String::new(),
            sta_id: 0,
        }
    }
    pub fn set_request_dev_id(&mut self, value: &str) {
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }

    pub fn set_request_sta_id(&mut self, value: i32) {
        self.sta_id = value;
    }
    pub fn get_sta_id(&mut self) -> i32{
        self.sta_id
    }

    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["sta_id"] = self.sta_id.into();
        let tem_str = data.dump() + "\r\n";
        print!("{}", tem_str);
        tem_str
    }

 
}

// 回复 的 ModBus配置信息
#[derive(Debug)]

struct ModbusConfInfo {
    pkg_code: i32,
    dev_id: String,
    mb_addr: i32,
    mb_baud: i32,
    mb_fmt: i8,
    channel_count: [i8; 4],
    channel_nums: i8,
    battery_nums: [i8; 4],
}
#[derive(Debug)]
struct ModbusConfData {
    seg_nums: i32, //mb寄存器地址段数
    start_addr: [i16; 10],
    length: [i8; 10],
}
#[derive(Debug)]
pub struct ResponseModbusConfInfo {
    respond_info: ModbusConfInfo,
    respond_data: ModbusConfData,
}

impl ResponseModbusConfInfo {
    pub fn new() -> ResponseModbusConfInfo {
        ResponseModbusConfInfo {
            respond_info: ModbusConfInfo {
                pkg_code: 0x02,
                dev_id: String::new(),
                mb_addr: 0,
                mb_baud: 0,
                mb_fmt: 0,
                channel_nums: 0,                 //4
                channel_count: [0; 4],
                battery_nums: [0; 4],
            },
            respond_data: ModbusConfData {
                seg_nums: 0,
                start_addr: [0; 10],
                length: [0; 10],
            },
        }
    }

    pub fn respond_json_to_mb_struct(&mut self, value:&str) -> i32 {
        let parsed:json::JsonValue;
        // println!("{:?}", value);
        // let value_str = json::stringify(&value);
        // println!("{:?}",json::parse(&value));
        match json::parse(value){
            Ok(val) => {
                println!("parse ok");
                parsed = val
            },
            Err(_) => {
                println!("no parse value");
                return -1;
            }
        }
        //fn parse(value:&str) -> Result<JaonValue>
        match parsed["pkg_code"].as_i32() {
            Some(n) => self.respond_info.pkg_code = n,
            None => {
                print!("can not get pkg_code");
                return -3;
            }
        }
        match parsed["dev_id"].as_str() {
            Some(n) => self.respond_info.dev_id.push_str(n),
            None => {
                print!("can not get dev_id");
                return -2;
            }
        }
        match parsed["mb_addr"].as_i32() {
            Some(n) => self.respond_info.mb_addr = n,
            None => {
                print!("can not get mb_addr");
                return -3;
            }
        }

        match parsed["mb_baud"].as_i32() {
            Some(n) => self.respond_info.mb_baud = n,
            None => {
                print!("can not get mb_baud");
                return -4;
            }
        }
        match parsed["mb_fmt"].as_i8() {
            Some(n) => self.respond_info.mb_fmt = n,
            None => {
                print!("can not get mb_fmt");
                return -5;
            }
        }
        let mut count = 0;
        self.respond_info.channel_nums = 0;
        for i in parsed["channel_count"].members() {
            match i.as_i8() {
                Some(n) => {
                    self.respond_info.channel_count[count] = n;
                    self.respond_info.channel_nums = self.respond_info.channel_nums + 1;
                }
                None => {
                    print!("can not get channel_count");
                    return -6;
                }
            }
            count = count + 1;
        }
        count = 0;
        for i in parsed["battery_nums"].members() {
            match i.as_i8() {
                Some(n) => self.respond_info.battery_nums[count] = n,
                None => {
                    print!("can not get battery_nums");
                    return -7;
                }
            }
            count = count + 1;
        }
        match parsed["seg"]["seg_nums"].as_i32() {
            Some(n) => self.respond_data.seg_nums = n,
            None => {
                print!("can not get seg_nums");
                return -8;
            }
        }
        count = 0;
        for i in parsed["seg"]["start_addr"].members() {
            // println!("{}",i);
            match i.as_i16() {
                Some(n) => self.respond_data.start_addr[count] = n,
                None => {
                    print!("can not get start_addr\n");
                    return -9;
                }
            }
            count += 1;
        }
        count = 0;
        for i in parsed["seg"]["length"].members() {
            match i.as_i8() {
                Some(n) => self.respond_data.length[count] = n,
                None => {
                    print!("can not get length");
                    return -10;
                }
            }
            count += 1;
        }
        return 0;
    }
    pub fn get_slave_addr(&mut self) -> u8 {
        self.respond_info.mb_addr as u8
    }

    // pub fn get_dev_id(&mut self) -> String{
    //     self.respond_info.dev_id
    // }

    /**************************/
    pub fn get_reg_start_addr(&mut self, location: i32) -> u16 {
        //返回的是数组 传入的是u16
        self.respond_data.start_addr[location as usize] as u16 //把开始地址中第一个数取出来,表示从这个地址开始读
    } //读多少? seg_nums?


    pub fn get_reg_data_num(&mut self) -> u16 {
        //以上传到mb中的
        self.respond_data.seg_nums as u16
    }

    pub fn get_length(&mut self, location: i32) -> i32 {
        self.respond_data.length[location as usize] as i32
    }


    pub fn get_channel_nums(&self) -> i32 {
        self.respond_info.channel_nums as i32
    }

    pub fn get_channel_count(&self, value: i32) -> i32 {    //假如有4个通道 value=1 就等到第二个通道的通道数
        self.respond_info.channel_count[value as usize] as i32
    }

    pub fn get_battery_nums(&self, value: i32) -> i32 {      //第value个通道的battery_nums
        self.respond_info.battery_nums[value as usize] as i32
    }










    // pub fn get_channel_nums(&self) -> i32 {
    //     self.info.channel_nums as i32
    // }
    // pub fn get_channel_count(&self, value: i32) -> i32 {
    //     self.info.channel_count[value as usize] as i32
    // }
    // pub fn get_battery_nums(&self, value: i32) -> i32 {
    //     self.info.battery_nums[value as usize] as i32
    // }
}







//上报modbus数据信息
struct ModbusConfIf {
    pkg_code: i32,
    sta_id: i32, //通信盒所属电站id
    netdev_code: i32, //网络类别 -1:无 0:以太网 1:wifi
    dev_id: String, //逆变器id
    sec: i32, //设备端时间
    seg_nums: i32,
    start_addr: Vec<i32>,
    length: Vec<i32>,
}
struct ModbusConf {
    data: Vec<i32>,
}
pub struct ReportModbusConfInfo {
    report_info: ModbusConfIf,
    data_length: ModbusConf,
}
impl ReportModbusConfInfo {
    pub fn new() -> ReportModbusConfInfo {
        ReportModbusConfInfo {
            report_info: ModbusConfIf {
                pkg_code: 0x03,
                sta_id: 0, //无
                netdev_code: 1,
                dev_id: String::new(), //返回
                sec: 0, //无
                seg_nums: 0, //无
                start_addr: vec![0; 10], //start_addr
                length: vec![0; 10], //seg_nums
            },
            data_length: ModbusConf {
                data: vec![0; 30], //这些数据是mb去读逆变器的数据,返回的数据再写进去
            },
        }
    }
    pub fn set_report_dev_id(&mut self, value: &str) {
        self.report_info.dev_id.clear();
        self.report_info.dev_id.push_str(value);
    }
    pub fn set_report_sta_id(&mut self, value: i32) {
        self.report_info.sta_id = value;
    }
    pub fn set_report_sec(&mut self, value: i32) {
        self.report_info.sec = value;
    }
    pub fn set_seg_nums(&mut self, value: i32) {
        //决定循环的次数
        self.report_info.seg_nums = value;
    }



    pub fn set_report_start_addr(&mut self, value: i32, location: i32){
        self.report_info.start_addr[location as usize] = value as i32;
    }
    pub fn set_report_length(&mut self, value: i8, location: i32) {
        self.report_info.length[location as usize] = value as i32; // length[] = respond_seg_nums:i32
    }

    pub fn set_report_data(&mut self, value: i32, location: i32) {
        self.data_length.data[location as usize] = value as i32;
    }

    // pub fn set_report_data_one(&mut self,value:i8,location:i32){
    //     self.data_length.data_one[location as usize] = value as i32;
    // }
    // pub fn set_report_data_two(&mut self,value:i8,location:i32){
    //     self.data_length.data_two[location as usize] = value as i32;
    // }
    // pub fn set_report_data_three(&mut self,value:i8,location:i32){
    //     self.data_length.data_three[location as usize] = value as i32;
    // }
    pub fn struct_to_json(&self) -> String {
        let mut report_data = json::JsonValue::new_object();
        report_data["pkg_code"] = self.report_info.pkg_code.into();
        report_data["sta_id"] = self.report_info.sta_id.into();
        report_data["netdev_code"] = self.report_info.netdev_code.into();
        report_data["dev_id"] = self.report_info.dev_id.clone().into();
        report_data["sec"] = self.report_info.sec.into();
        report_data["seg_nums"] = self.report_info.seg_nums.into();
        report_data["seg"]["start_addr"] = json::JsonValue::new_array();
        report_data["seg"]["length"] = json::JsonValue::new_array();
        // report_data["seg"]["data"][0] = json::JsonValue::new_array();          //data长度也是不固定的,怎么搞?先定义为3个吧
        // report_data["seg"]["data"][1] = json::JsonValue::new_array();
        // report_data["seg"]["data"][2] = json::JsonValue::new_array();


        for i in 0..self.report_info.seg_nums {
            //seg_nums地址段数为2 则从每一段的开始地址读length长度
            report_data["seg"]["data"][i as usize] = json::JsonValue::new_array();
            report_data["seg"]["start_addr"].push(self.report_info.start_addr[i as usize]).unwrap();
            report_data["seg"]["length"].push(self.report_info.length[i as usize]).unwrap();
            let n = self.report_info.length[i as usize];
            // println!("n = {}",n);
            for j in 0..n{
                // println!("j = {}",j);
                report_data["seg"]["data"][i as usize].push(self.data_length.data[j as usize]).unwrap();
            }
        }
        let tem_str = report_data.dump() + "\r\n";
        print!("{}\n", tem_str);
        tem_str
    }
}











//上报电池板信息
#[derive(Clone)]
struct BatteryInfo {
    pkg_code: i32,                  //"0x01"
    netdev_code: i32,
    dev_id: String,             //逆变器的id               
    sta_id: i32,                //通信盒所属电站id
    sec: i32,                   //设备端时间
    cluster_count: i8,          //本通信盒监测电池板串数
    cluster_index: i8,             //当前第几串电池板
    battery_nums: i8,           //当前串上的电池板个数
}

#[derive(Clone)]
struct BatteryData {
    addr: Vec<i8>,              //电池板地址
    status: Vec<i32>,           //“状态”高16位为软件版本信息，低16位为状态信息
    voltage: Vec<i32>,          //电压 10mv
    temp: Vec<i32>,             //温度 高16位为硬件版本信息，低16位为状态信息
    current: Vec<i16>,          //电流 ma
    power: Vec<i32>,                //功率 w
    energy: Vec<i32>,           //发电量wh
    signal: Vec<i32>,           //高16位为噪声，低16位为信号
}
//此命令无相应

#[derive(Clone)]
struct LastBatteryinfo {
    temp: Vec<i8>,
    count: Vec<i8>,
}
#[derive(Clone)]
pub struct ReportBatteryInfo {
    info: BatteryInfo,
    data: BatteryData,
    last: LastBatteryinfo,
}
impl ReportBatteryInfo {
    pub fn new() -> ReportBatteryInfo {
        ReportBatteryInfo {
            info: BatteryInfo {
                pkg_code: 0x1,
                dev_id: String::new(),
                netdev_code: 0,
                sta_id: 0,
                sec: 0,
                cluster_count: 0,
                cluster_index: 0,
                battery_nums: 0,
            },
            data: BatteryData {
                addr: vec![0; MAX_BATTERY],
                status: vec![0; MAX_BATTERY],        //high 16 bit sw_version  low 16bit status
                voltage: vec![0; MAX_BATTERY],
                temp: vec![0; MAX_BATTERY],         //high 16 bit hw_version  low 16bit temp
                current: vec![0; MAX_BATTERY],
                power: vec![0; MAX_BATTERY],
                energy: vec![0; MAX_BATTERY],
                signal: vec![0; MAX_BATTERY], // retain: vec![0; MAX_BATTERY],
            },
            last: LastBatteryinfo {
                temp: vec![0; MAX_BATTERY],
                count: vec![-1; MAX_BATTERY],
            },
        }
    }
    pub fn check_online(&mut self) {
        for i in 0..MAX_BATTERY {
            if self.data.addr[i as usize] != 0 {
                //online is 0
                self.last.count[i as usize] = 0;
                //temp or status
                self.last.temp[i as usize] = self.data.temp[i as usize] as i8;
            } else {
                if self.last.count[i as usize] != -1 && self.last.count[i as usize] <= 6 {
                    self.data.temp[i as usize] = self.last.temp[i as usize] as i32;
                    self.last.count[i as usize] = self.last.count[i as usize] + 1;
                }
            }
        }
    }
    pub fn get_current(&self, location: i32) -> i16 {
        self.data.current[location as usize]
    }
    pub fn get_voltage(&self, location: i32) -> i32 {
        self.data.voltage[location as usize]
    }

    
    pub fn set_dev_id(&mut self, value: &str) {
        self.info.dev_id.clear();
        self.info.dev_id.push_str(value);
    }
    pub fn set_sta_id(&mut self, value: i32) {
        self.info.sta_id = value;
    }
    pub fn set_netdev_code(&mut self, value: i32) {
        self.info.netdev_code = value;
    }
    pub fn set_sec(&mut self, value: i32) {
        self.info.sec = value;
    }
    pub fn set_cluster_count(&mut self, value: i8) {
        self.info.cluster_count = value;
    }
    pub fn set_cluster_index(&mut self, value: i8) {
        self.info.cluster_index = value;
    }
    pub fn set_battery_nums(&mut self, value: i8) {
        self.info.battery_nums = value;
    }
    pub fn set_addr_a(&mut self, value: i8, location: i32) {
        self.data.addr[location as usize] = value;
    }
    pub fn set_status_a(&mut self, value: i32, location: i32) {
        self.data.status[location as usize] = value;
    }
    pub fn set_voltage_a(&mut self, value: i32, location: i32) {
        self.data.voltage[location as usize] = value;
    }
    pub fn set_temp_a(&mut self, value: i32, location: i32) {
        self.data.temp[location as usize] = value;
    }
    pub fn set_current_a(&mut self, value: i16, location: i32) {
        self.data.current[location as usize] = value;
    }
    pub fn set_power_a(&mut self, value: i32, location: i32) {
        self.data.power[location as usize] = value;
    }
    pub fn set_energy_a(&mut self, value: i32, location: i32) {
        self.data.energy[location as usize] = value;
    }
    pub fn set_signal_a(&mut self, value: i32, location: i32) {
        self.data.signal[location as usize] = value;
    }



    //10a
    pub fn check_high_current_warning(&self, num: i32) -> String {//num is which string has fault
        let mut is_warning: bool = false;
        let mut warn_value: i16 = 0;
        
        for i in 0..self.info.battery_nums {
            if self.data.current[i as usize].abs() > 10000 && self.data.addr[i as usize] != 0 {
                is_warning = true;
                warn_value = self.data.current[i as usize];
            }
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();  //which string fault
            data["warn"] = 0x0020.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            let mut _t = data["battery_index"].push(0);
            _t = data["battery_warns"].push(0);
            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    //0.2 - 1a
    pub fn check_low_current_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value: i16 = 0;
        let mut num_t: i8 = 0;
        let mut count: i8 = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.current[i as usize].abs() < 1000 &&
                   self.data.current[i as usize].abs() > 256 {
                    warn_value = self.data.current[i as usize];
                    count = count + 1;
                }
            }
        }
        if (count < num_t / 4) && count != 0 {     //一串当中至少要有4组不是低电流才不报警
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0001.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            let mut _t = data["battery_index"].push(0);
            _t = data["battery_warns"].push(0);
            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    //0.2a
    pub fn check_no_current_warning(&self, num: i32) -> String {          //上报第几串无电流警告
        let mut is_warning: bool = false;
        let mut warn_value: i16 = 0;
        let mut num_t: i8 = 0;
        let mut count: i8 = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.current[i as usize].abs() <= 256 {
                    warn_value = self.data.current[i as usize];
                    count = count + 1;
                }
            }
        }
        if (count < num_t / 4) && count != 0 {
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0000.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            let mut _t = data["battery_index"].push(0);
            _t = data["battery_warns"].push(0);

            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    //50v
    pub fn check_high_voltage_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value = 0;
        let mut warn_value1: Vec<i32> = Vec::new();
        let mut warn_value2: Vec<i32> = Vec::new();
        let mut count: usize = 0;
        let mut num_t: usize = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.voltage[i as usize].abs() > 5000 {
                    warn_value = self.data.voltage[i as usize];
                    warn_value1.push((i + 1).into());
                    warn_value2.push(0x0080.into());
                    count = count + 1;
                }
            }
        }
        if count < (num_t / 4) && count != 0 {
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0080.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            for i in 0..count {
                let mut _r = data["battery_index"].push(warn_value1[i]);
                _r = data["battery_warns"].push(warn_value2[i]);
            }

            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    //20v
    pub fn check_low_voltage_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value = 0;
        let mut warn_value1: Vec<i32> = Vec::new();
        let mut warn_value2: Vec<i32> = Vec::new();
        let mut count: usize = 0;
        let mut num_t: usize = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.voltage[i as usize].abs() < 2000 {//10mv
                    warn_value = self.data.voltage[i as usize];
                    warn_value1.push((i + 1).into());
                    warn_value2.push(0x0040.into());
                    count = count + 1;
                }
            }
        }
        if count < (num_t / 4) && count != 0 {
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0040.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            for i in 0..count {
                let mut _r = data["battery_index"].push(warn_value1[i]);
                _r = data["battery_warns"].push(warn_value2[i]);
            }

            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    pub fn check_high_temp_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value = 0;
        let mut warn_value1: Vec<i32> = Vec::new();
        let mut warn_value2: Vec<i32> = Vec::new();
        let mut count: usize = 0;
        for i in 0..self.info.battery_nums {
            if (self.data.temp[i as usize] & 0b1111111111111111) > 80 &&
               self.data.addr[i as usize] != 0 {
                is_warning = true;
                warn_value = self.data.temp[i as usize];
                warn_value1.push((i + 1).into());
                warn_value2.push(0x0100.into());
                count = count + 1;
            }
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0100.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            for i in 0..count {
                let mut _r = data["battery_index"].push(warn_value1[i]);
                _r = data["battery_warns"].push(warn_value2[i]);
            }
            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }




//each battery data
    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.info.pkg_code.into();
        data["netdev_code"] = self.info.netdev_code.into();
        data["dev_id"] = self.info.dev_id.clone().into();
        data["sta_id"] = self.info.sta_id.into();
        data["sec"] = self.info.sec.into();
        data["cluster_count"] = self.info.cluster_count.into();
        data["cluster_index"] = self.info.cluster_index.into();
        data["battery_nums"] = self.info.battery_nums.into();
        data["addr"] = json::JsonValue::new_array();
        data["status"] = json::JsonValue::new_array();
        data["voltage"] = json::JsonValue::new_array();
        data["temp"] = json::JsonValue::new_array();
        data["current"] = json::JsonValue::new_array();
        data["power"] = json::JsonValue::new_array();
        data["energy"] = json::JsonValue::new_array();
        data["signal"] = json::JsonValue::new_array();
        for i in 0..self.info.battery_nums {              //0..4
            let mut _t = data["addr"].push(self.data.addr[i as usize]);
            _t = data["status"].push(self.data.status[i as usize]);
            _t = data["voltage"].push(self.data.voltage[i as usize]);
            _t = data["temp"].push(self.data.temp[i as usize]);
            _t = data["current"].push(self.data.current[i as usize]);
            _t = data["power"].push(self.data.power[i as usize]);
            _t = data["energy"].push(self.data.energy[i as usize]);
            _t = data["signal"].push(self.data.signal[i as usize]);
        }
        data.dump() + "\r\n"
    }
}










// 请求受时
pub struct RequestTimeInfo {
    pkg_code: i32,              //0x00
    dev_id: String,             //通信盒id
    f103_soft: u16,             //软件
    f103_hard: u16,
    f030_soft: u16,
    f030_hard: u16,
    com_soft: u16,
    com_hard: u16,
    no_current: u16,
    low_current: u16,
    high_current: u16,
    over_current: u16,
    low_voltage: u16,
    high_voltage: u16,
    high_temp: u16,
    eletric_arc: u16,
}

// // 响应 	
// 	{
// 	"pkg_code":"num",				//	"0x0"	
// 	"timestamp":"num",				//	"UNIX 时间戳,精确到秒 "	
// 	"sta_id":		"num"		    //	 本通信盒所属电站的 id ,如果返回值为 -1, 则 id 无效,不要上传数据 	
// 	}

impl RequestTimeInfo {
    pub fn new() -> RequestTimeInfo {
        RequestTimeInfo {
            pkg_code: 0x0,
            dev_id: String::new(),
            f030_hard: 0,
            f030_soft: 0,
            f103_hard: 0,
            f103_soft: 0,
            com_hard: 0,                         //65535  through modbus to read regi  0xFF80 system_hardware_info
            com_soft: 0,
            no_current: 256,
            low_current: 1000,
            high_current: 10000,
            over_current: 2000,
            low_voltage: 1500,
            high_voltage: 5000,
            high_temp: 80,
            eletric_arc: 88,
        }
    }
    pub fn set_dev_id(&mut self, value: &str) {
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }

    pub fn set_030_hard(&mut self,value:u16){
        self.f030_hard = value;
    }
    pub fn set_030_soft(&mut self,value:u16){
        self.f030_soft = value;
    }
    pub fn set_103_hard(&mut self,value:u16){
        self.f103_hard = value;
    }
    pub fn set_103_soft(&mut self,value:u16){
        self.f103_soft = value;
    }
    
    pub fn set_com_hard(&mut self, value: u16) {
        self.com_hard = value;
    }
    pub fn get_com_hard(&self) -> u16 {
        self.com_hard
    }




    pub fn set_no_current(&mut self, value: u16) {
        self.no_current = value;
    }
    pub fn get_no_current(&self) -> u16 {
        self.no_current
    }
    pub fn set_low_current(&mut self, value: u16) {
        self.low_current = value;
    }
    pub fn get_low_current(&self) -> u16 {
        self.low_current
    }
    pub fn set_high_current(&mut self, value: u16) {
        self.high_current = value;
    }
    pub fn get_high_current(&self) -> u16 {
        self.high_current
    }
    pub fn set_over_current(&mut self, value: u16) {
        self.over_current = value;
    }
    pub fn get_over_current(&self) -> u16 {
        self.over_current
    }
    pub fn set_low_voltage(&mut self, value: u16) {
        self.low_voltage = value;
    }
    pub fn get_low_voltage(&self) -> u16 {
        self.low_voltage
    }
    pub fn set_high_voltage(&mut self, value: u16) {
        self.high_voltage = value;
    }
    pub fn get_high_voltage(&self) -> u16 {
        self.high_voltage
    }
    pub fn set_high_temp(&mut self, value: u16) {
        self.high_temp = value;
    }
    pub fn get_high_temp(&self) -> u16 {
        self.high_temp
    }
    pub fn set_eletric_arc(&mut self, value: u16) {
        self.eletric_arc = value;
    }
    pub fn get_eletric_arc(&self) -> u16 {
        self.eletric_arc
    }


    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["f103_soft"] = self.f103_soft.into();
        data["f103_hard"] = self.f103_hard.into();
        data["f030_soft"] = self.f030_soft.into();
        data["f030_hard"] = self.f030_hard.into();
        data["com_soft"] = self.com_soft.into();
        data["com_hard"] = self.com_hard.into();
        data["no_current"] = self.no_current.into();
        data["low_current"] = self.low_current.into();
        data["high_current"] = self.high_current.into();
        data["over_current"] = self.over_current.into();
        data["low_voltage"] = self.low_voltage.into();
        data["high_voltage"] = self.high_voltage.into();
        data["high_temp"] = self.high_temp.into();
        data["eletric_arc"] = self.eletric_arc.into();
        let tem_str = data.dump() + "\n";
        print!("{}\n", tem_str);
        tem_str
    }
}





//return of request time_info
pub struct ResponseTimeInfo {
    pkg_code: i32,
    timestamp: i32,
    sta_id: i32,
}
impl ResponseTimeInfo {
    pub fn new() -> ResponseTimeInfo {
        ResponseTimeInfo {
            pkg_code: 0,
            timestamp: 0,
            sta_id: 0,
        }
    }
    pub fn get_sta_id(&self) -> i32 {
        self.sta_id
    }
    pub fn get_timestamp(&self) -> i32 {
        self.timestamp
    }
    pub fn json_to_struct(&mut self, value: &str) -> i32 {
        let parsed = json::parse(value).unwrap();

        match parsed["pkg_code"].as_i32() {
            Some(n) => self.pkg_code = n,
            None => {
                print!("can not get pkg_code");
                return -1;
            }
        }
        match parsed["timestamp"].as_i32() {
            Some(n) => self.timestamp = n,
            None => {
                print!("can not get timestamp");
                return -2;
            }
        }
        match parsed["sta_id"].as_i32() {
            Some(n) => self.sta_id = n,
            None => {
                print!("can not get sta_id");
                return -3;
            }
        }
        return 0;
    }
}












//inverter total info
pub struct RequestTotalPower {
    pkg_code: i8,
    dev_id: String,
    sta_id: i32,
    sec: i32,
    energy: i32,
    power: i32,
}
impl RequestTotalPower {
    pub fn new() -> RequestTotalPower {
        RequestTotalPower {
            pkg_code: 0x5,
            dev_id: String::new(),
            sta_id: 0,
            sec: 0,
            energy: 0,
            power: 0,
        }
    }
    pub fn get_energy(&self) -> i32 {
        self.energy
    }
    pub fn set_dev_id(&mut self, value: &str) {
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }
    pub fn set_sta_id(&mut self, value: i32) {
        self.sta_id = value;
    }
    pub fn set_sec(&mut self, value: i32) {
        self.sec = value;
    }
    pub fn set_energy(&mut self, value: i32) {
        self.energy = value;
    }
    pub fn set_power(&mut self, value: i32) {
        self.power = value;
    }
    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["sta_id"] = self.sta_id.into();
        data["sec"] = self.sec.into();
        data["energy"] = self.energy.into();
        data["power"] = self.power.into();
        data.dump() + "\r\n"
    }
}





//设置报警参数
pub struct ResponseSetWarning {
    pkg_code: i8,                       //0x06
    no_current: i16,        //ma
    low_current: i16,
    high_current: i32,
    over_current: i32,
    low_voltage: i32,    //10mv
    high_voltage: i32,
    high_temp: i8,          //c
}

impl ResponseSetWarning {
    pub fn new() -> ResponseSetWarning {
        ResponseSetWarning {
            pkg_code: 0x6,
            no_current: 0,
            low_current: 0,
            high_current: 0,
            over_current: 0,
            low_voltage: 0,
            high_voltage: 0,
            high_temp: 0,
        }
    }
    pub fn json_to_struct(&mut self, value: &str) -> i32 {
        let parsed;
        match json::parse(&value) {
            Ok(val) => parsed = val,
            Err(_) => {
                return -1;
            }
        }
        match parsed["pkg_code"].as_i8() {
            Some(n) => self.pkg_code = n,
            None => {
                print!("can not get pkg_code");
                return -2;
            }
        }
        match parsed["no_current"].as_i16() {
            Some(n) => self.no_current = n,
            None => {
                print!("can not get no_current");
                return -3;
            }
        }
        match parsed["low_current"].as_i16() {
            Some(n) => self.low_current = n,
            None => {
                print!("can not get low_current");
                return -4;
            }
        }
        match parsed["high_current"].as_i32() {
            Some(n) => self.high_current = n,
            None => {
                print!("can not get high_current");
                return -5;
            }
        }
        match parsed["over_current"].as_i32() {
            Some(n) => self.over_current = n,
            None => {
                print!("can not get over_current");
                return -6;
            }
        }
        match parsed["low_voltage"].as_i32() {
            Some(n) => self.low_voltage = n,
            None => {
                print!("can not get low_voltage");
                return -7;
            }
        }
        match parsed["high_voltage"].as_i32() {
            Some(n) => self.high_voltage = n,
            None => {
                print!("can not get high_voltage");
                return -8;
            }
        }
        match parsed["high_temp"].as_i8() {
            Some(n) => self.high_temp = n,
            None => {
                print!("can not get high_temp");
                return -9;
            }
        }
        return 0;
    }

    pub fn check_no_current_warning(&self, value: i32) -> bool {
        if self.no_current as i32 != value {
            true
        } else {
            false
        }
    }
    pub fn check_low_current_warning(&self, value: i32) -> bool {
        if self.no_current as i32 != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_current_warning(&self, value: i32) -> bool {
        if self.high_current != value {
            true
        } else {
            false
        }
    }
    pub fn check_over_current_warning(&self, value: i32) -> bool {
        if self.over_current != value {
            true
        } else {
            false
        }
    }
    pub fn check_low_voltage_warning(&self, value: i32) -> bool {
        if self.low_voltage != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_voltage_warning(&self, value: i32) -> bool {
        if self.high_voltage != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_temp_warning(&self, value: i32) -> bool {
        if self.high_temp as i32 != value {
            true
        } else {
            false
        }
    }
    pub fn get_no_current(&self) -> i32 {
        self.no_current as i32
    }
    pub fn get_low_current(&self) -> i32 {
        self.low_current as i32
    }
    pub fn get_high_current(&self) -> i32 {
        self.high_current
    }
    pub fn get_over_current(&self) -> i32 {
        self.over_current
    }
    pub fn get_low_voltage(&self) -> i32 {
        self.low_voltage
    }
    pub fn get_high_voltage(&self) -> i32 {
        self.high_voltage
    }
    pub fn get_high_temp(&self) -> i32 {
        self.high_temp as i32
    }
}