extern crate std;


use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::io::Seek;
use std::io;
use std::io::SeekFrom;
use std::prelude::*;
pub const FLASH_FIRM_VERSION: i32 = 0;
pub const FLASH_FIRM_NUMBER: i32 = 4;
pub const FLASH_FIRM_START: i32 = 4096;
pub const FLASH_TOTAL_ENERGY: i32 = 307200;
pub const FLASH_BATTERY_ENERGY_01: i32 = 307208; 
pub const FLASH_BATTERY_ENERGY_02: i32 = 307216;  //differ before 14
pub const FLASH_BATTERY_ENERGY_03: i32 = 307224;
pub const FLASH_BATTERY_ENERGY_04: i32 = 307232;
pub const FLASH_BATTERY_ENERGY_05: i32 = 307240;
pub const FLASH_BATTERY_ENERGY_06: i32 = 307248;
pub const FLASH_BATTERY_ENERGY_07: i32 = 307256;
pub const FLASH_BATTERY_ENERGY_08: i32 = 307264;
pub const FLASH_BATTERY_ENERGY_09: i32 = 307272;
pub const FLASH_BATTERY_ENERGY_10: i32 = 307280;
pub const FLASH_BATTERY_ENERGY_11: i32 = 307288;
pub const FLASH_BATTERY_ENERGY_12: i32 = 307296;
pub const FLASH_BATTERY_ENERGY_13: i32 = 307304;
pub const FLASH_BATTERY_ENERGY_14: i32 = 307312;
pub const FLASH_BATTERY_ENERGY_15: i32 = 307320;
pub const FLASH_BATTERY_ENERGY_16: i32 = 307328;
pub const FLASH_BATTERY_ENERGY_17: i32 = 307336;
pub const FLASH_BATTERY_ENERGY_18: i32 = 307344;
pub const FLASH_BATTERY_ENERGY_19: i32 = 307352;
pub const FLASH_BATTERY_ENERGY_20: i32 = 307360;
pub const FLASH_BATTERY_WARNING_NO_CURRENT: i32 = 307368;
pub const FLASH_BATTERY_WARNING_LOW_CURRENT: i32 = 307376;
pub const FLASH_BATTERY_WARNING_HIGH_CURRENT: i32 = 307384;
pub const FLASH_BATTERY_WARNING_OVER_CURRENT: i32 = 307392;
pub const FLASH_BATTERY_WARNING_LOW_VOLTAGE: i32 = 307400;
pub const FLASH_BATTERY_WARNING_HIGH_VOLTAGE: i32 = 307408;
pub const FLASH_BATTERY_WARNING_HIGH_TEMP: i32 = 307416;
pub const FLASH_BATTERY_WARNING_ELETRIC_ARC: i32 = 307424;
pub const FLASH_BATTERY_INFO_NUM: i32 = 308224;
pub const FLASH_BATTERY_INFO_START: i32 = 309248;

pub struct FlashOpt {
    fd: File,
}
impl FlashOpt {
    pub fn new(filename: &str) -> Result<FlashOpt, ()> {
        let f = File::open(filename);
        match f {
            Ok(ok) => Ok(FlashOpt { fd: ok }),
            Err(_) => Err(()),
        }
    }
    pub fn read_file(&mut self, buf: &mut [u8], location: u64) -> io::Result<usize> {       
        let res = self.fd.seek(SeekFrom::Start(location));    //偏移量
        match res {
            Ok(_) => (),
            Err(err) => {
                print!("seek:: {:?}\n", err);
                return Err(err);
            }
        }
        self.fd.read(buf)
    }

    pub fn write_file(&mut self, buf: &[u8], location: u64) -> io::Result<usize> {
        let res = self.fd.seek(SeekFrom::Start(location));
        match res {
            Ok(_) => (),
            Err(err) => {
                print!("seek:: {:?}\n", err);
                return Err(err);
            }
        }
        self.fd.write(buf)
    }
}