#include <nuttx/config.h>
#include <errno.h>
#include <debug.h>
#include <syslog.h>
#include <string.h>   //memcpy
#include <nuttx/arch.h>
#include <arch/board/board.h>
#include <arch/board/cbm_pvp.h>
#include <arch/chip/irq.h>
#include <semaphore.h>

#include "chip.h"
#include "stm32_adc.h"
#include "stm32_dma.h"
#include "up_arch.h"
#include "gilgal-cmb.h"


#define dmesg()	syslog("%s - %d\n", __FUNCTION__, __LINE__)
#define mesg syslog

#ifndef CONFIG_STM32_TIM1
#  error "This logic requires CONFIG_STM32_TIM1"
#endif

#ifndef CONFIG_STM32_FORCEPOWER
#  error "This logic requires CONFIG_STM32_FORCEPOWER"
#endif

#define FUD_IN_LEN 16

/* the unit of followed defs is uS */
#define FUD_IN_T0	800 		
#define FUD_IN_T1	1200		
#define FUD_IN_T2	1600		

static uint8_t   fud_in_d[FUD_IN_LEN];
static uint8_t   fud_in_len;	   /* need send when >0 */
static uint8_t   fud_in_status;
static cbm_pvp_t cbm_pvp;

static void cbm_pvp_update(void){
	#ifdef CONFIG_DEBUG_PVP
	cbm_pvp.pvp_data_len = fud_in_len;
	#endif
	if(fud_in_status == FUD_IN_NORMAL){				//update cbm_pvp
		if(fud_in_len >= 8){
			cbm_pvp.voltage = fud_in_d[0];
			cbm_pvp.voltage <<=8;
			cbm_pvp.voltage |= fud_in_d[1];
			cbm_pvp.hw_version = fud_in_d[2];
			cbm_pvp.hw_version <<= 8;
			cbm_pvp.hw_version |= fud_in_d[3];
			cbm_pvp.sw_version = fud_in_d[6];
			cbm_pvp.sw_version <<= 8;
			cbm_pvp.sw_version |= fud_in_d[7];
			cbm_pvp.cb_info = fud_in_d[4];
			cbm_pvp.cb_info <<= 8;
			cbm_pvp.cb_info |= fud_in_d[5];
			cbm_pvp.status = FUD_IN_NORMAL;
		}else{
			cbm_pvp.status = CBM_PVP_TOOSHORT;
		}
	}else{
		cbm_pvp.status = fud_in_status;
	}
}

static int cbm_pvp_isr(int irq, void *context){
	static uint8_t fud_in_seq;
	static uint8_t fud_in_buf[FUD_IN_LEN];
	uint32_t sr = getreg32(STM32_TIM1_SR);
	uint16_t ccr = getreg32(STM32_TIM1_CCR1);
	uint8_t in_bit;
	
	putreg32(~sr, STM32_TIM1_SR);
	if(sr & ATIM_SR_TIF){//captured

		if(fud_in_status == FUD_IN_NOTSTART || fud_in_status == FUD_IN_TIMEOUT){
			fud_in_status = FUD_IN_NORMAL;
		}
		if(fud_in_status == FUD_IN_NORMAL && ccr < FUD_IN_T1){ //get bit 0 or 1
			if(ccr < FUD_IN_T0) 
				in_bit = 0;
			else
				in_bit = 1;
			if(fud_in_seq < 8){
				fud_in_buf[fud_in_len] >>= 1;
				if(in_bit)
					fud_in_buf[fud_in_len] |= 0x80;
			}else if(fud_in_seq == 8){
				if(((fud_in_buf[fud_in_len]>>7) + (fud_in_buf[fud_in_len]>>6)+
					(fud_in_buf[fud_in_len]>>5) + (fud_in_buf[fud_in_len]>>4)+
					(fud_in_buf[fud_in_len]>>3) + (fud_in_buf[fud_in_len]>>2) +
					(fud_in_buf[fud_in_len]>>1) + fud_in_buf[fud_in_len] +
					in_bit) & 1) { //parity check err
					fud_in_status = FUD_IN_ERR_PARITY;
				}
			}else{
				fud_in_status = FUD_IN_ERR_BYTE_LONG;
			}
			if(fud_in_seq < 0xff)
				fud_in_seq++;
		}else if(fud_in_status == FUD_IN_NORMAL && ccr < FUD_IN_T2) {//get gap1
			if(fud_in_seq != 9){
				fud_in_status = FUD_IN_ERR_BYTE_SHORT;
			}
			fud_in_seq = 0;
			
			fud_in_len++;
			if(fud_in_len >= FUD_IN_LEN){
				fud_in_status = FUD_IN_ERR_FRAME_LONG;
			}
		}
		if(ccr >= FUD_IN_T2){ // get gap2
			if((fud_in_seq != 9 && fud_in_seq != 0)
			   && fud_in_status == FUD_IN_NORMAL){
				fud_in_status = FUD_IN_ERR_GAP2;
			}

			if(fud_in_seq == 9 && fud_in_status == FUD_IN_NORMAL){
				for(fud_in_seq = 1; fud_in_seq <= fud_in_len; fud_in_seq++){// integraty check
					fud_in_buf[0] ^= fud_in_buf[fud_in_seq];
				}
				if(fud_in_buf[0] != 0){// check err
					fud_in_status = FUD_IN_ERR_SUM;
				}else{//check pass, then resort
					for(fud_in_seq = 0; fud_in_seq < fud_in_len; fud_in_seq++){
						fud_in_d[fud_in_seq] = fud_in_buf[fud_in_len - fud_in_seq];
					}
				}
				cbm_pvp_update();
			}else if(fud_in_status != FUD_IN_NORMAL){
				cbm_pvp_update();
			}
			
			fud_in_status = FUD_IN_NORMAL;
			fud_in_len = 0;
			fud_in_seq = 0;
		}
	}else if(sr & ATIM_SR_UIF){// counter overflow
		fud_in_status = FUD_IN_TIMEOUT;
		fud_in_len = 0;
		fud_in_seq = 0;

		cbm_pvp_update();
	}
	return OK;
}





int cbm_pvp_start(void){
	static bool initialized = false;
	irqstate_t flags;
	
	uint16_t cr1;
	uint16_t ccmr1;
	uint16_t ccer;
	uint16_t smcr;
	
	if (!initialized){
        flags = enter_critical_section();
		{// configure pins		
			stm32_configgpio(GPIO_TIM1_CH1IN);
			
		}
		{// set cr1, disable counter
			putreg32(0, STM32_TIM1_CR1);
		}
		{// load prescaler
			putreg32(STM32_APB2_TIM1_CLKIN/1000000 - 1, STM32_TIM1_PSC); /* 1Mhz */
			putreg32(ATIM_EGR_UG, STM32_TIM1_EGR); /* Generate an update event */
		}
		{// set auto reload register
			putreg32(60000, STM32_TIM1_ARR); /* 60mS */
		}
		{// set CCMR register, input, IC1 is mapped on TI1
			ccmr1 =0;
			ccmr1 = (ATIM_CCMR_CCS_CCIN1 << ATIM_CCMR1_CC1S_SHIFT) |
				(ATIM_CCMR_ICPSC_NOPSC << ATIM_CCMR1_IC1PSC_SHIFT) |
				(ATIM_CCMR_ICF_FDTSd328 << ATIM_CCMR1_IC1F_SHIFT); /* ~10uS */
			putreg32(ccmr1, STM32_TIM1_CCMR1);
		}
		{// set slave mode control register
			smcr = ATIM_SMCR_TI1FP1;
			putreg32(smcr, STM32_TIM1_SMCR);
			smcr |= ATIM_SMCR_RESET;
			putreg32(smcr, STM32_TIM1_SMCR);
		}
		{// set interrupt, and enable capture and counter reload interrupt
			irq_attach(STM32_IRQ_TIM1UP, cbm_pvp_isr);
			up_enable_irq(STM32_IRQ_TIM1UP);
			putreg32(ATIM_DIER_UIE, STM32_TIM1_DIER);
		}
		{// set CCER capture/compare enable register
			ccer = ATIM_CCER_CC1E;
			ccer |= ATIM_CCER_CC1P; /* failing edge */
			putreg32(ccer, STM32_TIM1_CCER);
   		}
		{  /* Start the timer */
			cr1 = getreg32(STM32_TIM1_CR1);
			cr1 |= ATIM_CR1_CEN;
			putreg32(cr1, STM32_TIM1_CR1);
		}
		initialized = true;
		
        leave_critical_section(flags);
	}

	return 1;
}

int cbm_get_pvp(cbm_pvp_t *pvp){      //传入一个结构体指针
	irqstate_t flags;
	flags = enter_critical_section();
	memcpy(pvp, &cbm_pvp, sizeof(cbm_pvp_t));
	leave_critical_section(flags);
	return OK;
}
/*********************************
内存拷贝函数
从source所指内存地址拷贝n个字节到des t所指内存地址的开始位置
void *memcpy(void *dest,const void*src,size_t n);  //返回指向dest的指针
memcpy会覆盖原有数据，如果追加数据，要将目标地址增加到追加数据的地址
**********************************************/
