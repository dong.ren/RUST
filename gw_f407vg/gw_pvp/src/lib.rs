
#![no_std]
// #[macro_use]
extern crate std;
use std::*;
/*
rust c parameter dont't use struct pointer
*/

#[derive(Debug)]
pub struct CbmPvp{
    pub pvp_data_len: u8,
    pub hw_version: u16,
    pub sw_version: u16,
    pub voltage: u16,
    pub cb_info: u16,
    pub status: u8,
}

extern "C" {
    pub fn cbm_pvp_start() -> i8;
    pub fn cbm_get_pvp(port:*mut CbmPvp) -> i8;
    pub fn power_check_24v() -> i8;             //高1 低0 
}

impl CbmPvp {
    //let x = Point{x:0,y:0};
    pub fn new() -> CbmPvp {
        CbmPvp {
            pvp_data_len: 0,
            hw_version: 0, 
            sw_version: 0,
            voltage: 0,
            cb_info: 0,        //温度和电池信息
            status: 0,
        }
    }
    pub fn pvp_start(&mut self) -> i8{
        unsafe {
            cbm_pvp_start()
        }
    }
    pub fn get_pvp(&mut self) -> i8 {
        unsafe {
             cbm_get_pvp(self) 
        }
    }
    pub fn check_24_v(&mut self) -> i8{
        unsafe{
            power_check_24v()
        }
    }
    
}

// /************

// ******/
// fn pvp_go(pvp: ::CbmPvp::new()){
//     pvp.pvp_start();
//     pvp.get_pvp();
// }
// fn check_24_v(pvp: ::CbmPvp::new()) -> i8{
//     pvp.check_24_v()
// }
