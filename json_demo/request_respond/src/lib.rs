
extern crate json_struct;

use std::*;
use std::net::*;
use std::io::Write;
use std::io::Read;
use std::thread;
use std::string::String;
use std::str;



/*
完成上传和解析函数
*/
pub fn report_request_parse(report_data: &mut json_struct::RequestTimeInfo,respond_data: &mut json_struct::ResponseTimeInfo){

    // println!("report_request_parse go");
    let mut stream = TcpStream::connect("139.224.35.75:4445")
                        .expect("Couldn't connect to the server...");
    // println!("服务器连接成功");
    //建立接收buf
    let mut recv_from_the_server = vec![0u8;500];
    println!("{}",report_data.struct_to_json().len());
    let request_send = stream.write(&(report_data.struct_to_json().as_bytes()));
    // println!("发送成功");
    //<usize,std::io::Error>
    let send_request:usize;
    match request_send{
        Ok(ok) => send_request = ok,
        _ => unreachable!()
    }
    thread::sleep(std::time::Duration::from_millis(100));
    let res_recv = stream.read(&mut recv_from_the_server);
    // println!("{:?}",recv_from_the_server);
    let str_recv:usize;
    match res_recv {
        Ok(ok) => {
            str_recv = ok;
            println!("str_recv leng is = {}",str_recv);
        }
        Err(_) => {
            print!("can not understand the server info!\n");
            return;
        }
    }
    let mut res = String::new();
    unsafe{
    let buf1 = String::from_raw_parts(recv_from_the_server.as_mut_ptr(),str_recv,str_recv); 
        // println!("buf1 = {}",buf1);
        if buf1.contains("\n"){
            let _temp_str = buf1.clone();
            let v:Vec<&str> = _temp_str.splitn(2,'\n').collect();
            res.push_str(&v[1]);
            //去掉读取的字节数,只打印返回的数
            print!("res is {}",res);
            if res.contains("pkg_code"){ 
            // println!("返回mb信息接受成功");
            }
        }
    }     
    unsafe{
    let x = respond_data.json_to_struct(&res);       //str_recv(读取的字节数) or &res(json字符)
    if x < 0{
        println!("parse error");
        return ;
        }
    }


}



pub fn battery_data_handle(report_battery_info:&mut json_struct::ReportBatteryInfo,time_for_energy:& i32,
                                            buf: &mut [u8;18],i:i32,current:&mut[u16;4]){
    if buf[1] != 0{ 
        let battery_addr:i32 = buf[0] as i32;                                                          
        report_battery_info.set_addr_a((battery_addr+1) as i8,battery_addr);
        let voltage = (buf[2] as i32) << 8 | buf[3] as i32;   
        report_battery_info.set_voltage_a(voltage,battery_addr);
        report_battery_info.set_cluster_index((i + 1) as i8);         
        let power = voltage * current[i as usize] as i32;          
        report_battery_info.set_power_a(power as i32,battery_addr);

        report_battery_info.set_current_a(current[i as usize] as i16,battery_addr);
        
        
        //energy
        // let sum_energy = power * time_for_energy;
        // report_battery_info.set_energy_a(sum_energy,battery_addr);
        // println!("4");
        
        let noise = (buf[8] as u16) << 8 | buf[9] as u16;
        let signal = (buf[10] as u16) << 8 | buf[11] as u16;
        let noise_signal:i32 = (noise as i32) << 16| signal as i32;       //高16位为噪声，低为信号
        report_battery_info.set_signal_a(noise_signal,battery_addr);
        let sw_version = (buf[6] as u16) << 8 | buf[7] as u16;
        report_battery_info.set_status_a((sw_version as i32) << 16 | buf[5] as i32 ,battery_addr);
        let hw_version = (buf[12] as u16) << 8 | buf[13] as u16;
        report_battery_info.set_temp_a((hw_version as i32) << 16 | buf[4] as i32,battery_addr);
        // let mut battery_sn_high = (buf[14] as u32) << 8 | buf[15] as u32;
        // let mut battery_sn_low = (buf[16] as u32) << 8 | buf[17] as u32;

        // report_battery_info.check_online();            //在线检测
        report_battery_info.struct_to_json();
    }else{
        println!("modbus data error!");
    }

}

pub fn time_data_handle(report_time: &mut json_struct::RequestTimeInfo,buf: &mut Vec<u8>){
    let mut f030_soft_version = (buf[0] as u16) << 8 | buf[1] as u16;
    report_time.set_030_soft(f030_soft_version);
    let mut f030_hard_version = (buf[2] as u16) << 8 | buf[3] as u16;
    report_time.set_030_hard(f030_hard_version);
    let mut f103_hard_version = f030_hard_version;
    report_time.set_103_hard(f103_hard_version);
    let mut f103_soft_version = (buf[6] as u16) << 8 | buf[7] as u16;
    report_time.set_103_soft(f103_soft_version);
    println!("f030_soft_version = {}", f030_soft_version);
    println!("f030_hard_version = {}", f030_hard_version);
    println!("f103_soft_version = {}", f103_soft_version);
    println!("f103_hard_version = {}", f103_hard_version);
   

}


pub fn report_mb_data_handle(report_mb_info: &mut json_struct::ReportModbusConfInfo,buf: &mut Vec<u8>){
    for i in 0..4{
    report_mb_info.set_report_start_addr( 0x5000 as i32 + (9 * i) as i32,i as i32);  //0x5000 0x5009 0x5012 0x501b
    report_mb_info.set_report_length(9 as i8,i as i32); //[9,9,9,9]
    for m in 0..9{  
        report_mb_info.set_report_data((buf[m as usize] as i32) << 8 | (buf[m as usize + 1 ])as i32,i as i32);
    }
    }
    report_mb_info.struct_to_json();
    // println!("{:?}",report_mb_info);

}

