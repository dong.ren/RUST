

#[warn(unused_variables)] //编译标记

extern crate modbus;
extern crate json_struct;
extern crate request_respond;

use std::*;
// use std::net::*;
// use std::io::Write;
// use std::io::Read;
use std::thread;
// use std::string::String;
use std::str;
use std::sync::{Arc,Mutex};
// use std::sync::mpsc;


fn main()
{

    let dev_id:&str = "1064852484630";
    let sta_id:i32 = 1;
        /**************************
        1.请求授时 记录时间
        2.请求modbus配置信息
        3.上报modbus信息中有时间，记录这之间的时间，计算发电量
        4.上传电池信息
        ***************************/
    let mut request_mb_info: json_struct::RequestModbusConfInfo = json_struct::RequestModbusConfInfo::new();
    let mut respond_mb_info:json_struct::ResponseModbusConfInfo = json_struct::ResponseModbusConfInfo::new();
    let mut report_mb_info: json_struct::ReportModbusConfInfo = 
                ::json_struct::ReportModbusConfInfo::new();

    let mut report_time: json_struct::RequestTimeInfo = 
                ::json_struct::RequestTimeInfo::new();
    let mut respond_time_info: json_struct::ResponseTimeInfo = 
                ::json_struct::ResponseTimeInfo::new();

    let mut report_battery_info: json_struct::ReportBatteryInfo = 
                ::json_struct::ReportBatteryInfo::new();
                


    // let mut request_firm_data = json_struct::RequestFirmDate::new();
    // let mut respond_firm_data = json_struct::ResponseFirmUpdate::new();

    report_time.set_dev_id(dev_id);
    request_respond::report_request_parse(&mut report_time,&mut respond_time_info);

    let mut time_stamp = respond_time_info.get_timestamp();
    println!("time_stamp one is {:?}",time_stamp);
    

    
    /*****************************************************/

    // //   buf是共享资源，对其进行保护
    // let mut buf = Arc::new(Mutex::new(vec![0u8;50]));
    // // let mut buf = vec![0u8;50];

    // //软件版本号   
    // let mut f030_soft_version = (buf[0] as u16) << 8 | buf[1] as u16;
    // report_time.set_030_soft(f030_soft_version);
    // let mut f030_hard_version = (buf[2] as u16) << 8 | buf[3] as u16;
    // report_time.set_030_hard(f030_hard_version);
    // let mut f103_hard_version = f030_hard_version;
    // report_time.set_103_hard(f103_hard_version);
    // let mut f103_soft_version = (buf[6] as u16) << 8 | buf[7] as u16;
    // report_time.set_103_soft(f103_soft_version);
    // println!("f030_soft_version = {}", f030_soft_version);
    // println!("f030_hard_version = {}", f030_hard_version);
    // println!("f103_soft_version = {}", f103_soft_version);
    // println!("f103_hard_version = {}", f103_hard_version);
    // // let mut buf = vec![0u8;50];
    // //电弧报警阀值
    // let mut eletric_arc = (buf[0] as u16) << 8 | buf[1] as u16;
    // report_time.set_eletric_arc(eletric_arc);
    // println!("eletric_arc = {}",eletric_arc);
    // report_time.set_dev_id(dev_id);
    // // let mut buf = vec![0u8;50];
    // println!("{:?}",report_time);
    // println!("timestamp two is {:?}",respond_time_info.get_timestamp());    




    
    //获取通道电流
    //电弧报警信息
    // let mut current = [0u16;10];
    // for i in 0..4{
    //     current[i as usize] = ((buf[2 * i as usize]) as u16) << 8 | (buf[1 + 2 * i as usize]) as u16;
    //     // println!("{:?}",current[i as usize]);
    // }
    
    /**************************************************************************************/
    //获取每个太阳能电池板检测数据
    //上报每个电池信息
//     report_battery_info.set_dev_id(dev_id);
//     report_battery_info.set_sta_id(sta_id);
//     report_battery_info.set_netdev_code(0);
//     report_battery_info.set_sec(respond_time_info.get_timestamp());
//     report_battery_info.set_cluster_count(4 as i8);                  //本通信盒检测电池板串数 = 回复modbus中channel_nums
//     report_battery_info.set_battery_nums(40 as i8);                 //当前串上电池板个数
//     //modbus 配置信息
//     report_mb_info.set_report_dev_id(dev_id);     //和请求dev_id的值一样
//     report_mb_info.set_report_sta_id(sta_id);     //和请求sta_id一样   
//     report_mb_info.set_report_sec(respond_time_info.get_timestamp());      
//     report_mb_info.set_seg_nums(9);        //9 
    
//     let mut current = [1,2,3,4];
//     // for(i,j) in 
// for i in 0..4{
//     // let buf = Arc::new(Mutex::new(vec![0u8;20]));
//     let mut buf = [1,2,3,4,5,6,7,8,9,9,8,7,6,5,4,3,2,1];
//     // let buf = rand::thread_rng().gen_range(0, 5);
//     // println!("{:?}",&buf);
//     // let buf = buf.clone();
//     let handle = thread::spawn( move || {
//         // let mut buf = buf.lock().unwrap();
//         let mut report_battery_info: json_struct::ReportBatteryInfo = 
//                 json_struct::ReportBatteryInfo::new();
//         let mut respond_time_info: json_struct::ResponseTimeInfo = json_struct::ResponseTimeInfo::new();
//         let mut report_time: json_struct::RequestTimeInfo = json_struct::RequestTimeInfo::new();
//         // request_respond::report_request_parse(&mut report_time,&mut respond_time_info);
//         let mut time_for_energy = respond_time_info.get_timestamp() - time_stamp;
//         // println!("{}",time_for_energy);
//         request_respond::battery_data_handle(&mut report_battery_info,&time_for_energy,&mut buf,i,&mut current);

//         let mut report_mb_info: json_struct::ReportModbusConfInfo = 
//                 ::json_struct::ReportModbusConfInfo::new();
        
//         // request_respond::report_mb_data_handle( &mut report_mb_info,&mut buf);
//         //drop(buf)  //lock的ｂｕｆ在离开作用域就解锁了
//     });
//     handle.join().unwrap();        
// }
// println!("here");
    
        //     //连接服务器
        // let mut stream = TcpStream::connect("139.224.35.75:4445")
        //                 .expect("Couldn't connect to the server...");
        // println!("battery服务器连接成功");


        // //上报电池板信息；
        // stream.write(&(report_battery_info.struct_to_json().as_bytes()));




        // let mut stream = TcpStream::connect("139.224.35.75:4445")
        //                 .expect("Couldn't connect to the server...");
        // println!("mb服务器连接成功");

        // //report mb_data
        // stream.write(&(report_mb_info.struct_to_json().as_bytes()));
        // thread::sleep(std::time::Duration::from_millis(100));
        // println!("request_respond_report ok!");
    
    

}






