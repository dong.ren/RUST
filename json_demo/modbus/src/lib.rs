// //use std::prelude::*;
// #![feature(collections)]
use std::*;
use std::fs;
use std::fs::File;
use std::io::Read;
// use std::io::*;
use std::io::Write;
//use collections::string::ToString;
use std::time;
use std::thread;
pub struct ModBus {
    fd: File,
}
impl ModBus {
    // pub fn new(path: &str) -> Self {
    //     let _fd = fs::File::open(path);
    //     //let f = _fd.unwrap();
    //     Ok(ModBus { fd: _fd });
    // }
    pub fn new(path: &str) -> io::Result<ModBus> {
        let _fd = fs::File::open(path).unwrap();
        //let f = File::from_raw_fd(_fd);
        Ok(ModBus { fd: _fd })
    }

    //Datalen应该是指字节.CRC校验函数
    fn crc16(&mut self, pushmsg: &[u8], datalen: u8) -> u16 {
        //let mut i = 0;
        //let mut j = 0;
        let mut crc_sumx = 0xffff;
        // let mut c = 0;
        for i in 0..datalen {
            let c = (pushmsg[i as usize] & 0x00ff) as u16;
            crc_sumx ^= c;
            for _ in 0..8 {
                if (crc_sumx & 0x0001) > 0 {
                    crc_sumx >>= 1;
                    crc_sumx ^= 0xA001;
                } else {
                    crc_sumx >>= 1;
                }
            }
        }
        return crc_sumx;
    }

    pub fn modbus_write(&mut self, buf: &[u8]) -> usize {
        let rec = self.fd.write(buf).unwrap();
        return rec;
    }

    pub fn modbus_read(&mut self, buf: &mut [u8]) -> usize {
        let rec = self.fd.read(buf).unwrap();
        return rec;
    }

    //功能码03;RTU模式
    //slave_addr:表示从机地址, reg_start_addr:表示要读取寄存器的起始地址,也就是要读取的第一个寄存器的地址, data_len:表示要读取几个寄存器(寄存器个数)
    //read_mult_reg_input:表示功能码03,读取多路寄存器的输入值.opt为1时表示返回值只是从机返回的值,opt为2时，表示返回值
    //包括主机发送+从机返回的。
    pub fn read_holding_reg(
        &mut self,
        slave_addr: u8,                //mb_addr
        reg_start_addr: u16,            //start_addr
        reg_data_num: u16,              //seg_nums
        buf: &mut [u8],                 //接收返回i16的数组
        opt: u8,
        ) -> i16 {
        let func_code: u8 = 03;
        let send_data: [u8; 6] = [
            slave_addr,
            func_code,
            ((reg_start_addr & 0xff00) >> 8) as u8,
            (reg_start_addr & 0xff) as u8,
            ((reg_data_num & 0xff00) >> 8) as u8,
            (reg_data_num & 0xff) as u8,
        ];
        let send_data_crc: [u8; 8] = [
            slave_addr,
            func_code,
            ((reg_start_addr & 0xff00) >> 8) as u8,
            (reg_start_addr & 0xff) as u8,
            ((reg_data_num & 0xff00) >> 8) as u8,
            (reg_data_num & 0xff) as u8,
            (self.crc16(&send_data, 6) & 0xff) as u8, 
            ((self.crc16(&send_data, 6) & 0xff00) >> 8) as u8,
        ];
        print!("send_data_crc = {:?}\n", send_data_crc);
        //send_data[6] = ((self.crc16(&send_data, 6) & 0xff00) >> 8) as u8;
        //send_data[7] = (self.crc16(&send_data, 6) & 0xff) as u8;
        //let mut rec_buf: [u8; 7] = [0; 7];
        let mut rec_buf = vec![0; (5 + ((reg_data_num * 2) as u8)) as usize];
        let mut rec_buf_return = vec![0; (13 + ((reg_data_num * 2) as u8)) as usize];
        let _send_write = self.modbus_write(&send_data_crc);
        print!("send_write = {}\n", _send_write);
        if _send_write > 0 {
            print!("write success\n");
            thread::sleep(time::Duration::from_millis(
                (13 + (reg_data_num * 2)) as u64,
            ));
            if 1 == opt {
                print!("opt=1\n");
                
                self.modbus_read(&mut rec_buf);
                print!("rec_buf = {:?}\n", rec_buf);
                // print!("opt=2\n");
                if 1 > 0 {
                    print!("read success\n");
                    let mut rec_buf_part = vec![0; (3 + ((reg_data_num * 2) as u8)) as usize];
                    for i in 0..((3 + ((reg_data_num * 2) as u8)) as usize) {
                        rec_buf_part[i] = rec_buf[i];
                    }
                    let crc_rec_part_low: u8 =
                        (((self.crc16(&rec_buf_part, (3 + ((reg_data_num * 2) as u8)))) &
                              0xff00) >> 8) as u8;
                    let crc_rec_part_high: u8 =
                        (((self.crc16(&rec_buf_part, (3 + ((reg_data_num * 2) as u8)))) &
                            0xff)) as u8;
                    if (crc_rec_part_high == rec_buf[(3 + ((reg_data_num * 2) as u8)) as usize]) &&
                        (crc_rec_part_low == rec_buf[(4 + ((reg_data_num * 2) as u8)) as usize])
                    {
                        for i in 0..((reg_data_num * 2) as u8) as usize {
                            buf[i] = rec_buf[3 + i];
                        }
                        return (reg_data_num * 2) as i16;
                    } else {
                        return -1;
                    }
                } else {
                    print!("read error\n");
                    return -1;

                }
            } else if 2 == opt {
                self.fd.read(&mut rec_buf_return).unwrap();
                print!("rec_buf_return = {:?}\n", rec_buf_return);
                if 1 > 0 {
                    print!("read success\n");
                    let mut rec_buf_return_part =
                        vec![0; (3 + ((reg_data_num * 2) as u8)) as usize];
                    for i in 0..((3 + ((reg_data_num * 2) as u8)) as usize) {
                        rec_buf_return_part[i] = rec_buf_return[8 + i];
                    }
                    let crc_rec_part_low: u8 = ((self.crc16(
                        &rec_buf_return_part,
                        (3 + ((reg_data_num * 2) as u8)),
                    ) & 0xff00) >> 8) as u8;
                    let crc_rec_part_high: u8 =
                        ((self.crc16(&rec_buf_return_part, (3 + ((reg_data_num * 2) as u8))) &
                              0xff)) as u8;
                    if (crc_rec_part_high ==
                            rec_buf_return[(11 + ((reg_data_num * 2) as u8)) as usize]) &&
                        (crc_rec_part_low ==
                             rec_buf_return[(12 + ((reg_data_num * 2) as u8)) as usize])
                    {
                        for i in 0..((reg_data_num * 2) as u8) as usize {
                            buf[i] = rec_buf_return[11 + i];
                        }
                        return (reg_data_num * 2) as i16;
                    } else {
                        return -1;
                    }
                } else {
                    print!("read error\n");
                    return -1;
                }
            } else {
                return -2;
            }
        } else {
            print!("write error\n");
            return -1;
        }
    }


}
