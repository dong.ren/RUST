#![no_std]
extern crate json;
extern crate std;
use std::*; //使用引入的包里的模块和部分函数.
use std::prelude::*;
use std::convert::Into;
use std::clone::Clone;
use std::string::String;
pub const MAX_BATTERY: usize = 20;
//设置报警参数
pub struct SetWarning {
    pkg_code: i8,                       //0x06
    no_current: i16,        //ma
    low_current: i16,
    high_current: i32,
    over_current: i32,
    low_voltage: i32,    //10mv
    high_voltage: i32,
    high_temp: i8,          //c
}
impl SetWarning {
    pub fn new() -> SetWarning {
        SetWarning {
            pkg_code: 0x6,
            no_current: 0,
            low_current: 0,
            high_current: 0,
            over_current: 0,
            low_voltage: 0,
            high_voltage: 0,
            high_temp: 0,
        }
    }
    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["no_current"] = self.no_current.into();
        data["low_current"] = self.low_current.into();
        data["high_current"] = self.high_current.into();
        data["over_current"] = self.over_current.into();
        data["low_voltage"] = self.low_voltage.into();
        data["high_voltage"] = self.high_voltage.into();
        data["high_temp"] = self.high_temp.into();
        let tem_str = data.dump() + "\r\n";
        print!("{}\n", tem_str);
        tem_str
    }
    pub fn json_to_struct(&mut self, value: &str) -> i32 {
        let parsed;
            //json::parse = object!{}  
        match json::parse(&value) {
            Ok(val) => parsed = val,
            Err(_) => {
                return -1;
            }
        }
        match parsed["pkg_code"].as_i8() {
            Some(n) => self.pkg_code = n,
            None => {
                print!("can not get pkg_code");
                return -2;
            }
        }
        match parsed["no_current"].as_i16() {
            Some(n) => self.no_current = n,
            None => {
                print!("can not get no_current");
                return -3;
            }
        }
        match parsed["low_current"].as_i16() {
            Some(n) => self.low_current = n,
            None => {
                print!("can not get low_current");
                return -4;
            }
        }
        match parsed["high_current"].as_i32() {
            Some(n) => self.high_current = n,
            None => {
                print!("can not get high_current");
                return -5;
            }
        }
        match parsed["over_current"].as_i32() {
            Some(n) => self.over_current = n,
            None => {
                print!("can not get over_current");
                return -6;
            }
        }
        match parsed["low_voltage"].as_i32() {
            Some(n) => self.low_voltage = n,
            None => {
                print!("can not get low_voltage");
                return -7;
            }
        }
        match parsed["high_voltage"].as_i32() {
            Some(n) => self.high_voltage = n,
            None => {
                print!("can not get high_voltage");
                return -8;
            }
        }
        match parsed["high_temp"].as_i8() {
            Some(n) => self.high_temp = n,
            None => {
                print!("can not get high_temp");
                return -9;
            }
        }
        return 0;
    }
    
    pub fn check_no_current_warning(&self, value: i32) -> bool {
        if self.no_current as i32 != value {
            true
        } else {
            false
        }
    }
    
    pub fn check_low_current_warning(&self, value: i32) -> bool {
        if self.no_current as i32 != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_current_warning(&self, value: i32) -> bool {
        if self.high_current != value {
            true
        } else {
            false
        }
    }
    pub fn check_over_current_warning(&self, value: i32) -> bool {
        if self.over_current != value {
            true
        } else {
            false
        }
    }
    pub fn check_low_voltage_warning(&self, value: i32) -> bool {
        if self.low_voltage != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_voltage_warning(&self, value: i32) -> bool {
        if self.high_voltage != value {
            true
        } else {
            false
        }
    }
    pub fn check_high_temp_warning(&self, value: i32) -> bool {
        if self.high_temp as i32 != value {
            true
        } else {
            false
        }
    }
    pub fn get_no_current(&self) -> i32 {
        self.no_current as i32
    }
    pub fn get_low_current(&self) -> i32 {
        self.low_current as i32
    }
    pub fn get_high_current(&self) -> i32 {
        self.high_current
    }
    pub fn get_over_current(&self) -> i32 {
        self.over_current
    }
    pub fn get_low_voltage(&self) -> i32 {
        self.low_voltage
    }
    pub fn get_high_voltage(&self) -> i32 {
        self.high_voltage
    }
    pub fn get_high_temp(&self) -> i32 {
        self.high_temp as i32
    }
}
/*
mb数据信息：
let data = json::parse(r#"
{
    "pkg_code":"0x03",
    "sta_id":0,
    "netdev_code":0,
    "dev_id":0,
    "sec":0,
    "seg_nums":0,
    "seg":{
        "star_addr":[0],
        "length":[0],
        "data":[[0],[0],[0]]
    }
}"#).unwrap();

prinln!("{:?}",data);
data.dump();
data["seg"]["data"][0] = "23";
data["seg"]["data"][1] = "32";
*/
//上报modbus数据信息
struct ModbusConfInfo {
    pkg_code: i32,
    sta_id:i32,
    netdev_code:i32,
    dev_id:i32,
    sec:i32,
    sec_nums:i32,
    start_addr:Vec<i32>,
    length:Vec<i32>,
}
struct ModbusConfData {
    data1:Vec<i32>,
    data2:Vec<i32>,
    data3:Vec<i32>,
}    
pub struct ReportModbusConfInfo {
    info: ModbusConfInfo,
    data: ModbusConfData,
}
impl ReportModbusConfInfo {
    pub fn new() -> ReportModbusConfInfo {
        ReportModbusConfInfo {
            info: ModbusConfInfo {
                pkg_code: 0x03,
                sta_id:0,
                netdev_code:0,
                dev_id:0,
                sec:0,
                sec_nums:0,
                start_addr:vec![0; 10],
                length:vec![0; 10],
            },
            data: ModbusConfData {
                data1:vec![0;10],
                data2:vec![0;10],
                data3:vec![0;10],
            },
        }
    }
    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.info.pkg_code.into();
        data["sta_id"] = self.info.sta_id.into();
        data["netdev_code"] = self.info.netdev_code.into();
        data["dev_id"] = self.info.dev_id.into();
        data["sec"] = self.info.sec.into();
        data["sec_nums"] = self.info.sec_nums.into();

        data["start_addr"] = json::JsonValue::new_array();
        data["length"] = json::JsonValue::new_array();
        data["data1"] = json::JsonValue::new_array();
        data["data2"] = json::JsonValue::new_array();
        data["data3"] = json::JsonValue::new_array();
        for i in 0..8{
            let mut _t = data["start_addr"].push(self.info.start_addr[i as usize]);
        _t = data["length"].push(self.info.length[i as usize]);
        _t = data["data1"].push(self.data.data1[i as usize]);
        _t = data["data2"].push(self.data.data2[i as usize]);
        _t = data["data3"].push(self.data.data3[i as usize]);
        }
        let tem_str = data.dump() + "\r\n";
        print!("{}\n", tem_str);
        tem_str
    }
    pub fn json_to_struct(&mut self, value: &str) -> i32 {
        let parsed;
        match json::parse(value) {            //fn parse(value:&str) -> Result<JaonValue>
            Ok(val) => parsed = val,
            Err(_) => {
                println!("no value");
                return -11;
            }
        }
        match parsed["pkg_code"].as_i32() {
            Some(n) => self.info.pkg_code = n,
            None => {
                print!("can not get pkg_code");
                return -1;
            }
        }
        match parsed["sta_id"].as_i32() {
            Some(n) => self.info.sta_id = n,
            None => {
                print!("can not get sta_id");
                return -1;
            }
        }
        match parsed["netdev_code"].as_i32() {
            Some(n) => self.info.netdev_code = n,
            None => {
                print!("can not get netdev_id");
                return -1;
            }
        }
        match parsed["dev_id"].as_i32() {
            Some(n) => self.info.dev_id = n,
            None => {
                print!("can not get dev_id");
                return -1;
            }
        }
        match parsed["sec"].as_i32() {
            Some(n) => self.info.sec = n,
            None => {
                print!("can not get sec");
                return -1;
            }
        }
        match parsed["sec_nums"].as_i32() {
            Some(n) => self.info.sec_nums = n,
            None => {
                print!("can not get sec_num");
                return -1;
            }
        }

        let mut count = 0;
        for i in parsed["seg"]["start_addr"].members() {
            match i.as_i32() {
                Some(n) => self.info.start_addr[count] = n,
                None => {
                    print!("can not get start_addr\n");
                    return -1;
                }
            }
        }
        for i in parsed["seg"]["length"].members() {
            match i.as_i32() {
                Some(n) => self.info.length[count] = n,
                None => {
                    print!("can not get length");
                    return -1;
                }
            }
        }
        for i in parsed["seg"]["data"]["data1"].members() {
            match i.as_i32() {
                Some(n) => self.data.data1[count] = n,
                None => {
                    print!("can not get data");
                    return -1;
                }
            }
        }
        for i in parsed["seg"]["data"]["data2"].members() {
            match i.as_i32() {
                Some(n) => self.data.data2[count] = n,
                None => {
                    print!("can not get data");
                    return -1;
                }
            }
        }
        for i in parsed["seg"]["data"]["data3"][0].members() {
            match i.as_i32() {
                Some(n) => self.data.data3[count] = n,
                None => {
                    print!("can not get data");
                    return -1;
                }
            }
        }
        return 0;
    }
}



//请求modbus配置信息
pub struct RequestModbusConfInfo {        
    pkg_code: i8,                      //0x02
    dev_id: String,                     //逆变器的id          1064852484630
    sta_id: i32,                         //0     
}
// //响应
// {
//     "pkg_code":"num",  //0x02
//     "dev_id":"num",
//     "mb_addr":"num",
//     "mb_baud":"num",
//     "mb_fmt":"num",          //"0:8N1,1:8N2,2:8E1,3:801"
//     "channel_count":["num"], //电池板通道数量
//     "battery_nums":["num"],    //每个通道分别有多少个电池板，元素与‘channel_count’各元素对应
//     "seg":e
//     {
//         "seg_num":"num",    //"mb寄存器地址段数
//         "start_addr":["num"],
//         "lengh":["num"]
//     }
// }
impl RequestModbusConfInfo {
    pub fn new() -> RequestModbusConfInfo {
        RequestModbusConfInfo {
            pkg_code: 0x2,
            dev_id: String::new(),
            sta_id: 0,
        }
    }
    pub fn set_dev_id(&mut self, value: &str) {
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }

    pub fn set_sta_id(&mut self, value: i32) {
        self.sta_id = value;
    }

    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["sta_id"] = self.sta_id.into();
        let tem_str = data.dump() + "\r\n";
        print!("{}\n", tem_str);
        tem_str
    }
}
/*
//上报电池板信息
#[derive(Clone)]
struct BatteryInfo {
    pkg_code: i32,                  //"0x01"
    netdev_code: i32,
    dev_id: String,             //逆变器的id               
    sta_id: i32,                //通信盒所属电站id
    sec: i32,                   //设备端时间
    cluster_count: i8,          //本通信盒监测电池板串数
    cluster_index: i8,          //本通信盒监测当前第几串电池板
    battery_nums: i8,           //当前串上的电池板个数
}
#[derive(Clone)]
struct BatteryData {
    addr: Vec<i8>,              //电池板地址
    status: Vec<i32>,           //“状态”高16位为软件版本信息，低16位为状态信息
    voltage: Vec<i32>,          //电压 10mv
    temp: Vec<i32>,             //温度 高16位为硬件版本信息，低16位为状态信息
    current: Vec<i16>,          //电流 ma
    power: Vec<i32>,            //功率 w
    energy: Vec<i32>,           //发电量wh
    signal: Vec<i32>,           //高16位为噪声，低16位为信号
}
//此命令无相应
#[derive(Clone)]
struct LastBatteryinfo {
    temp: Vec<i8>,
    count: Vec<i8>,
}
#[derive(Clone)]
pub struct RequestBatteryInfo {
    info: BatteryInfo,
    data: BatteryData,
    last: LastBatteryinfo,
}
impl RequestBatteryInfo {
    pub fn new() -> RequestBatteryInfo {
        RequestBatteryInfo {
            info: BatteryInfo {
                pkg_code: 0x1,
                dev_id: String::new(),
                netdev_code: 0,
                sta_id: 0,
                sec: 0,
                cluster_count: 0,
                cluster_index: 0,
                battery_nums: 0,
            },
            data: BatteryData {
                addr: vec![0; MAX_BATTERY],
                status: vec![0; MAX_BATTERY],
                voltage: vec![0; MAX_BATTERY],
                temp: vec![0; MAX_BATTERY],
                current: vec![0; MAX_BATTERY],
                power: vec![0; MAX_BATTERY],
                energy: vec![0; MAX_BATTERY],
                signal: vec![0; MAX_BATTERY], // retain: vec![0; MAX_BATTERY],
            },
            last: LastBatteryinfo {
                temp: vec![0; MAX_BATTERY],
                count: vec![-1; MAX_BATTERY],
            },
        }
    }
    pub fn check_online(&mut self) {
        for i in 0..MAX_BATTERY {            //for中i的默认类型是什么？   i32
            if self.data.addr[i as usize] != 0 {
                self.last.count[i as usize] = 0;
                self.last.temp[i as usize] = self.data.temp[i as usize] as i8;      //将数组里的值->i8     lastbatteryinfo
            } else {
                if self.last.count[i as usize] != -1 && self.last.count[i as usize] <= 6 {
                    self.data.temp[i as usize] = self.last.temp[i as usize] as i32;       //batterydata
                    self.last.count[i as usize] = self.last.count[i as usize] + 1;
                }
            }
        }
    }
    pub fn get_current(&self, location: i32) -> i16 {          //location?
        self.data.current[location as usize]
    }
    pub fn get_voltage(&self, location: i32) -> i32 {
        self.data.voltage[location as usize]
    }
    pub fn set_dev_id(&mut self, value: &str) {        //逆变器id
        self.info.dev_id.clear();
        self.info.dev_id.push_str(value);
    }
    pub fn set_sta_id(&mut self, value: i32) {         //通信盒所属电站id
        self.info.sta_id = value;
    }
    pub fn set_netdev_code(&mut self, value: i32) {
        self.info.netdev_code = value;
    }
    pub fn set_sec(&mut self, value: i32) {
        self.info.sec = value;
    }
    pub fn set_cluster_count(&mut self, value: i8) {
        self.info.cluster_count = value;
    }
    pub fn set_cluster_index(&mut self, value: i8) {
        self.info.cluster_index = value;
    }
    pub fn set_battery_nums(&mut self, value: i8) {        //当前串上电池板个数
        self.info.battery_nums = value;
    }
    pub fn set_addr_a(&mut self, value: i8, location: i32) {
        self.data.addr[location as usize] = value;
    }
    pub fn set_status_a(&mut self, value: i32, location: i32) {
        self.data.status[location as usize] = value;
    }
    pub fn set_voltage_a(&mut self, value: i32, location: i32) {
        self.data.voltage[location as usize] = value;
    }
    pub fn set_temp_a(&mut self, value: i32, location: i32) {
        self.data.temp[location as usize] = value;
    }
    pub fn set_current_a(&mut self, value: i16, location: i32) {
        self.data.current[location as usize] = value;
    }
    pub fn set_power_a(&mut self, value: i32, location: i32) {
        self.data.power[location as usize] = value;
    }
    pub fn set_energy_a(&mut self, value: i32, location: i32) {      //发电量
        self.data.energy[location as usize] = value;
    }
    pub fn set_signal_a(&mut self, value: i32, location: i32) {  //高16位噪声，低16位信号
        self.data.signal[location as usize] = value;
    }



    pub fn check_high_current_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value: i16 = 0;
        for i in 0..self.info.battery_nums {      //检查当前串上每一个电池板
            if self.data.current[i as usize].abs() > 10000 && self.data.addr[i as usize] != 0 {  //电流绝对值大于10a，为什么电池板地址要等于0？
                is_warning = true;
                warn_value = self.data.current[i as usize];
            }
        }
        if is_warning == false {
            String::new()      //建立新字符串？有什么用？
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0020.into();    //32 ??
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            let mut _t = data["battery_index"].push(0);
            _t = data["battery_warns"].push(0);
            let temp_str = data.dump() + "\n";       //为什么不是\r\n
            print!("{}", temp_str);
            temp_str
        }
    }
    //json::stringify()  -> string
    //json::JsonValue() -> json
    pub fn check_low_current_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value: i16 = 0;
        let mut num_t: i8 = 0;
        let mut count: i8 = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.current[i as usize].abs() < 1000 &&
                   self.data.current[i as usize].abs() > 256 {
                    warn_value = self.data.current[i as usize];
                    count = count + 1;
                }
            }
        }
        if (count < num_t / 4) && count != 0 { //这个条件不会成立
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0001.into(); //1
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();  //“[]” 数组外面加引号
            data["battery_warns"] = json::JsonValue::new_array();
            let mut _t = data["battery_index"].push(0);  //  "[0,"foo"]"
            _t = data["battery_warns"].push(0);

            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    pub fn check_no_current_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value: i16 = 0;
        let mut num_t: i8 = 0;
        let mut count: i8 = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.current[i as usize].abs() <= 256 {
                    warn_value = self.data.current[i as usize];
                    count = count + 1;
                }
            }
        }
        if (count < num_t / 4) && count != 0 {
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0000.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            let mut _t = data["battery_index"].push(0);
            _t = data["battery_warns"].push(0);

            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    pub fn check_high_voltage_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value = 0;
        let mut warn_value1: Vec<i32> = Vec::new();
        let mut warn_value2: Vec<i32> = Vec::new();
        let mut count: usize = 0;
        let mut num_t: usize = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.voltage[i as usize].abs() > 5000 {
                    warn_value = self.data.voltage[i as usize];
                    warn_value1.push((i + 1).into());
                    warn_value2.push(0x0080.into());
                    count = count + 1;
                }
            }
        }
        if count < (num_t / 4) && count != 0 {
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0080.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            for i in 0..count {
                let mut _r = data["battery_index"].push(warn_value1[i]);
                _r = data["battery_warns"].push(warn_value2[i]);
            }

            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    pub fn check_low_voltage_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value = 0;
        let mut warn_value1: Vec<i32> = Vec::new();
        let mut warn_value2: Vec<i32> = Vec::new();
        let mut count: usize = 0;
        let mut num_t: usize = 0;
        for i in 0..self.info.battery_nums {
            if self.data.addr[i as usize] != 0 {
                num_t = num_t + 1;
                if self.data.voltage[i as usize].abs() < 2000 {
                    warn_value = self.data.voltage[i as usize];
                    warn_value1.push((i + 1).into());
                    warn_value2.push(0x0040.into());
                    count = count + 1;
                }
            }
        }
        if count < (num_t / 4) && count != 0 {
            is_warning = true;
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0040.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            for i in 0..count {
                let mut _r = data["battery_index"].push(warn_value1[i]);
                _r = data["battery_warns"].push(warn_value2[i]);
            }

            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    pub fn check_high_temp_warning(&self, num: i32) -> String {
        let mut is_warning: bool = false;
        let mut warn_value = 0;
        let mut warn_value1: Vec<i32> = Vec::new();
        let mut warn_value2: Vec<i32> = Vec::new();
        let mut count: usize = 0;
        for i in 0..self.info.battery_nums {
            if (self.data.temp[i as usize] & 0b1111111111111111) > 80 &&
               self.data.addr[i as usize] != 0 {
                is_warning = true;
                warn_value = self.data.temp[i as usize];
                warn_value1.push((i + 1).into());
                warn_value2.push(0x0100.into());
                count = count + 1;
            }
        }
        if is_warning == false {
            String::new()
        } else {
            let mut data = json::JsonValue::new_object();
            data["pkg_code"] = 0x06.into();
            data["sta_id"] = self.info.sta_id.into();
            data["dev_id"] = self.info.dev_id.clone().into();
            data["sec"] = self.info.sec.into();
            data["cluster_index"] = num.into();
            data["warn"] = 0x0100.into();
            data["value"] = warn_value.into();
            data["battery_index"] = json::JsonValue::new_array();
            data["battery_warns"] = json::JsonValue::new_array();
            for i in 0..count {
                let mut _r = data["battery_index"].push(warn_value1[i]);
                _r = data["battery_warns"].push(warn_value2[i]);
            }
            let temp_str = data.dump() + "\n";
            print!("{}", temp_str);
            temp_str
        }
    }
    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.info.pkg_code.into();
        data["netdev_code"] = self.info.netdev_code.into();
        data["dev_id"] = self.info.dev_id.clone().into();
        data["sta_id"] = self.info.sta_id.into();
        data["sec"] = self.info.sec.into();
        data["cluster_count"] = self.info.cluster_count.into();
        data["cluster_index"] = self.info.cluster_index.into();
        data["battery_nums"] = self.info.battery_nums.into();
        data["addr"] = json::JsonValue::new_array();
        data["status"] = json::JsonValue::new_array();
        data["voltage"] = json::JsonValue::new_array();
        data["temp"] = json::JsonValue::new_array();
        data["current"] = json::JsonValue::new_array();
        data["power"] = json::JsonValue::new_array();
        data["energy"] = json::JsonValue::new_array();
        data["signal"] = json::JsonValue::new_array();
        for i in 0..self.info.battery_nums {
            let mut _t = data["addr"].push(self.data.addr[i as usize]);
            _t = data["status"].push(self.data.status[i as usize]);
            _t = data["voltage"].push(self.data.voltage[i as usize]);
            _t = data["temp"].push(self.data.temp[i as usize]);
            _t = data["current"].push(self.data.current[i as usize]);
            _t = data["power"].push(self.data.power[i as usize]);
            _t = data["energy"].push(self.data.energy[i as usize]);
            _t = data["signal"].push(self.data.signal[i as usize]);
        }
        data.dump() + "\r\n"
    }
}
// 请求受时
pub struct RequestTimeInfo {
    pkg_code: i32,              //0x00
    dev_id: String,             //通信盒id
    f103_soft: u16,             //软件
    f103_hard: u16,
    
    f030_soft: u16,
    f030_hard: u16,
    
    com_soft: u16,
    com_hard: u16,

    no_current: u16,
    low_current: u16,
    high_current: u16,
    over_current: u16,
    
    low_voltage: u16,
    high_voltage: u16,
    
    high_temp: u16,
    eletric_arc: u16,
}

// // 响应 	
// 	{
// 	"pkg_code":"num",				//	"0x0"	
// 	"timestamp":"num",				//	"UNIX 时间戳,精确到秒 "	
// 	"sta_id":		"num"		    //	 本通信盒所属电站的 id ,如果返回值为 -1, 则 id 无效,不要上传数据 	
// 	}
impl RequestTimeInfo {
    pub fn new() -> RequestTimeInfo {
        RequestTimeInfo {
            pkg_code: 0x0,
            dev_id: String::new(),
            f030_hard: 0,
            f030_soft: 0,
            f103_hard: 0,
            f103_soft: 0,
            com_hard: 0,
            com_soft: 0,
            no_current: 256,
            low_current: 1000,
            high_current: 10000,
            over_current: 2000,
            low_voltage: 1500,
            high_voltage: 5000,
            high_temp: 80,
            eletric_arc: 88,
        }
    }
    pub fn set_dev_id(&mut self, value: &str) {
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }
    pub fn set_no_current(&mut self, value: u16) {
        self.no_current = value;
    }
    pub fn get_no_current(&self) -> u16 {
        self.no_current
    }
    pub fn set_com_hard(&mut self, value: u16) {
        self.com_hard = value;
    }
    pub fn get_com_hard(&self) -> u16 {
        self.com_hard
    }
    pub fn set_low_current(&mut self, value: u16) {
        self.low_current = value;
    }
    pub fn get_low_current(&self) -> u16 {
        self.low_current
    }
    pub fn set_high_current(&mut self, value: u16) {
        self.high_current = value;
    }
    pub fn get_high_current(&self) -> u16 {
        self.high_current
    }
    pub fn set_over_current(&mut self, value: u16) {
        self.over_current = value;
    }
    pub fn get_over_current(&self) -> u16 {
        self.over_current
    }
    pub fn set_low_voltage(&mut self, value: u16) {
        self.low_voltage = value;
    }
    pub fn get_low_voltage(&self) -> u16 {
        self.low_voltage
    }
    pub fn set_high_voltage(&mut self, value: u16) {
        self.high_voltage = value;
    }
    pub fn get_high_voltage(&self) -> u16 {
        self.high_voltage
    }
    pub fn set_high_temp(&mut self, value: u16) {
        self.high_temp = value;
    }
    pub fn get_high_temp(&self) -> u16 {
        self.high_temp
    }
    pub fn set_eletric_arc(&mut self, value: u16) {
        self.eletric_arc = value;
    }
    pub fn get_eletric_arc(&self) -> u16 {
        self.eletric_arc
    }


    pub fn struct_to_json(&self) -> String {
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["f103_soft"] = self.f103_soft.into();
        data["f103_hard"] = self.f103_hard.into();
        data["f030_soft"] = self.f030_soft.into();
        data["f030_hard"] = self.f030_hard.into();
        data["com_soft"] = self.com_soft.into();
        data["com_hard"] = self.com_hard.into();
        data["no_current"] = self.no_current.into();
        data["low_current"] = self.low_current.into();
        data["high_current"] = self.high_current.into();
        data["over_current"] = self.over_current.into();
        data["low_voltage"] = self.low_voltage.into();
        data["high_voltage"] = self.high_voltage.into();
        data["high_temp"] = self.high_temp.into();
        data["eletric_arc"] = self.eletric_arc.into();
        let tem_str = data.dump() + "\r\n";
        print!("{}\n", tem_str);
        tem_str
    }
}




































//上报逆变器总功率信息
 struct ReportInverterPowerInfo{
     pkg_code:i32,       //0x05
     sta_id:i32,
     dev_id:String,
     sec:i32,
     energy:i32,    //wh
     power:i32,     //w
 }
impl ReportInverterPowerInfo{
    pub fn new() -> ReportInverterPowerInfo{
        ReportInverterPowerInfo{
            pkg_code:0x05,
            sta_id:0,
            dev_id:String::new(),
            sec:0,
            energy:0,
            power:0
        }
    }

    pub fn set_dev_id(&mut self, value: &str) {
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }

    pub fn set_sta_id(&mut self, value: i32) {
        self.sta_id = value;
    } 
    
    pub fn set_sec(&mut self, value: i32) {
        self.sec = value;
    } 

    pub fn set_energy(&mut self, value: i32) {
        self.energy = value;
    }
    pub fn set_power(&mut self, value: i32) {
        self.power = value;
    }

    pub fn strucct_to_json(&self) -> String{
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["sta_id"] = self.sta_id.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["sec"] = self.sec.into();
        data["energy"] = self.energy.into();
        data["power"] = self.power.into();
        let tem_str = data.dump() +"\r\n";
        print!("{}", tem_str);
        tem_str
    }



}









//上报故障信息
// wain类型：
// //  0x0000  NO_CURRENT
// //	0x0001	LOW_CURRENT	
// //	0x0020	HIGH_CURRENT	
// //	0x0040	LOW_VOLTAGE	
// //	0x0080	HIGH_VOLTAGE	
// //	0x0100	HIGH_TEMP	
// //	0x0200	ELETRIC_ARC	
// //	0x0400	OVER_CURRENT

struct ReportFaultInfo{
    pkg_code:i32,           //0x06
    sta_id:i32,
    dev_id:String,
    sec:i32,
    cluster_index:i32,          //第几串发生故障
    wain:i32,
    value:i32,                  //读取到的故障值，与电池板里的信息里的单位相同，复合类型的报警值此项忽略
    battery_index:Vec<i32>,       // 发生报警的电池板地址
    battery_warns:Vec<i32>,     //每个电池板的故障类型
}
impl ReportFaultInfo{
    pub fn new() -> ReportFaultInfo{
        ReportFaultInfo{
            pkg_code:0x06,
            sta_id:0,
            dev_id:String::new(),
            sec:0,
            cluster_index:0,
            wain:0,
            value:0,
            battery_index:vec![0;MAX_BATTERY],
            battery_warns:vec![0;MAX_BATTERY],
        }
    }

    pub fn set_dev_id(&mut self, value: &str) {
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }

    pub fn set_sta_id(&mut self, value: i32) {
        self.sta_id = value;
    }

    pub fn set_sec(&mut self, value: i32) {
        self.sec = value;
    }

    pub fn set_cluster_index(&mut self, value: i32) {
        self.cluster_index = value;
    }

    pub fn set_wain(&mut self, value: i32) {
        self.wain = value;
    }

    pub fn set_value(&mut self, value1: i32) {
        self.value = value1;
    }
    pub fn set_battery_index(&mut self, value: i32, location: i32) {
        self.battery_index[location as usize] = value;
    }
    pub fn set_battery_warns(&mut self, value: i32, location: i32) {
        self.battery_warns[location as usize] = value;
    }
    pub fn strucct_to_json(&self) -> String{
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["sta_id"] = self.sta_id.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["sec"] = self.sec.into();
        data["cluster_index"] = self.cluster_index.into();
        data["wain"] = self.wain.into();
        data["value"] = self.value.into();
        data["battery_index"] = json::JsonValue::new_array();
        data["battery_warns"] = json::JsonValue::new_array();
        let tem_str = data.dump() +"\r\n";
        print!("{}", tem_str);
        tem_str
    }

}















//请求改造电站的配置信息
pub struct RequestStationConfInfo{
    pkg_code:i32,
    dev_id:String,
}
impl RequestStationConfInfo{
    pub fn new () -> RequestStationConfInfo{
        RequestStationConfInfo{
            pkg_code:0x200,
            dev_id:String::new(),
        }

    }

    pub fn set_dev_id(&mut self,value:&str){
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }
    pub fn struct_to_json(&self) -> String{
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        let tem_str = data.dump() +"\r\n";
        print!("{}", tem_str);
        tem_str
    }

}








//上报改造电站的汇流箱数据
struct ReportFlowingData{
    pkg_code:i32,
    dev_id:String,
    sta_id:i32,
    inv_id:i32,     //逆变器的id
    node_id:i32,   //节点id
    ext_temp:i32,   //0.1‘c
    state:i32,     //开关量状态
    drain_current:i32,   //0.1ma
    voltage:i32,        //10mv
    current:Vec<i32>,     //10ma
    power:Vec<i32>,     //每个通道的功率
    all_current:i32,     //所有通道的总电流 10ma
    all_power:i32,      //所有通道的总功率 0.1w
}

impl ReportFlowingData{
    pub fn new() -> ReportFlowingData{
        ReportFlowingData{
            pkg_code:0x201,
            dev_id:String::new(),
            sta_id:0,
            inv_id:0,
            node_id:0,
            ext_temp:0,
            state:0,
            drain_current:0,
            voltage:0,
            current:vec![0;MAX_BATTERY],
            power:vec![0;MAX_BATTERY],
            all_current:0,
            all_power:0,
        } 
    }

    pub fn set_dev_id(&mut self,value:&str){
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }
    pub fn set_sta_id(&mut self, value: i32) {
        self.sta_id = value;
    }
    pub fn set_inv_id(&mut self, value: i32) {
        self.inv_id = value;
    }
    pub fn set_node_id(&mut self, value: i32) {
        self.node_id = value;
    }
    pub fn set_ext_temp(&mut self, value: i32) {
        self.ext_temp = value;
    }
    pub fn set_state(&mut self, value: i32) {
        self.state = value;
    }
    pub fn set_drain_current(&mut self, value: i32) {
        self.drain_current = value;
    }
    pub fn set_voltage(&mut self, value: i32) {
        self.voltage = value;
    }
    pub fn get_current(&self, location: i32) -> i32 {
        self.current[location as usize]
    }
    pub fn set_power(&mut self, value: i32, location: i32) {
        self.power[location as usize] = value;
    }
    pub fn set_all_current(&mut self, value: i32) {
        self.all_current = value;
    }
    pub fn set_all_power(&mut self, value: i32) {
        self.all_power = value;
    }


    pub fn strucct_to_json(&self) -> String{
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["sta_id"] = self.sta_id.into();
        data["inv_id"] = self.inv_id.into();
        data["node_id"] = self.node_id.into();
        data["ext_temp"] = self.ext_temp.into();
        data["state"] = self.state.into();
        data["drain_current"] = self.drain_current.into();
        data["voltage"] = self.voltage.into();
        data["current"] = json::JsonValue::new_array();
        data["power"] = json::JsonValue::new_array();
        data["all_current"] = self.all_current.into();
        data["all_power"] = self.all_power.into();
        let tem_str = data.dump() +"\r\n";
        print!("{}", tem_str);
        tem_str
    }


}








//上报改造电站的逆变器总信息
pub struct ReportInverterInfo{
    pkg_code:i32,      //0x202
    dev_id:String,
    sta_id:i32,
    inv_id:i32,          //逆变器id
    current:i32,        //10ma
    voltage:i32,        //10mv
    power:i32,           //逆变器总功率
}
impl ReportInverterInfo{
    pub fn new() -> ReportInverterInfo{
        ReportInverterInfo{
            pkg_code:0x202,
            dev_id:String::new(),
            sta_id:0,
            inv_id:0,
            current:0,
            voltage:0,
            power:0,
        }
    }
    pub fn set_dev_id(&mut self,value:&str){
        self.dev_id.clear();
        self.dev_id.push_str(value);
    }
    pub fn set_sta_id(&mut self, value: i32) {
        self.sta_id = value;
    }
    pub fn set_inv_id(&mut self, value: i32) {
        self.inv_id = value;
    }
    pub fn set_power(&mut self, value: i32) {
        self.power = value;
    }

    pub fn set_current(&mut self, value: i32) {
        self.current = value;
    }
    pub fn set_voltage(&mut self, value: i32) {
        self.voltage = value;
    }

    pub fn struct_to_json(&self) -> String{
        let mut data = json::JsonValue::new_object();
        data["pkg_code"] = self.pkg_code.into();
        data["dev_id"] = self.dev_id.clone().into();
        data["sta_id"] = self.sta_id.clone().into();
        data["inv_id"] = self.inv_id.clone().into();
        data["current"] = self.current.clone().into();
        data["voltage"] = self.voltage.clone().into();
        data["power"] = self.power.clone().into();
        let tem_str = data.dump() +"\r\n";
        print!("{}", tem_str);
        tem_str
    }
}

*/