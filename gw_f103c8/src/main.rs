
#![no_std] //预编译指令 不使用rust标准库, 这里我们使用自己封装好的库代替标准库
#![no_main] //预编译指令 不使用rust规定的main函数作为程序入口,这里我们的程序入口由系统运行前的C语言来指定
#[macro_use] //预编译指令 添加对打印的宏支持
extern crate std; //引入我们封装的标准库
extern crate gprs; //引入我们写的代码
use std::*; //使用引入的包里的模块和部分函数.
pub const DEV_NAME: &'static str = "/dev/ttyS1";
pub const SERVER_IP: &'static str = "139.224.35.75";
pub const SERVER_PORT:u32 = 4445;
pub const VALUE:&str = "0";
#[no_mangle] //让编译器不要修改函数名称  防止C代码找不到main函数
pub fn main() {
    println!("Here Begin!");
    let mut gp:gprs::GPRS = gprs::GPRS::new(DEV_NAME).unwrap();
    gp.gprs_init();
    gp.tcp_connect(SERVER_IP,SERVER_PORT);
    println!("end");

} 