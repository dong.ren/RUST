#![no_std]   //不使用rust标准库
#[macro_use] //添加对打印的宏支持
extern crate std;
extern crate json_struct;
use std::*;
use std::vec::Vec;
use std::fs::File;               //File::open()
use std::thread;
use std::time::Duration;
use std::string::String;
use std::io::Read;
use std::io::Write;
pub const VALUE:&str = "1064852484630";
pub struct GPRS{
    fd:File,
}
impl GPRS{
    pub fn new(dev_path: &str) -> io::Result<GPRS> {        
        let file = try!(File::open(dev_path));
        Ok(GPRS { fd: file })
    }
    pub fn check_write(&mut self,buf:&[u8]) ->io::Result<(usize)>{
             self.fd.write(buf)
    }
    pub fn check_read(&mut self) -> i8{
        let mut count = 0;
        let mut respond = vec![0u8;300];
        loop{
            let ret = self.fd.read(&mut respond);               
            match ret {
                Ok(ok) => unsafe {
                    let mut buf = String::from_raw_parts(respond.as_mut_ptr(), ok, ok);        //unsafe {fn from_raw_parts(buf:*mut u8,length:usize,capacity:usize) -> String} 
                    // print!("{}",buf);           //at的回复
                    if buf.contains("ERROR") {
                        let res_str = buf.clone();
                        mem::forget(buf);
                        let v:Vec<&str> = res_str.splitn(2, "\n").collect();           
                        let mut last_res: String = String::new();
                        last_res.push_str(&v[0]);
                        if last_res.contains("Network is already opened")
                        {
                            println!("network alreadly opened ");
                            return -1;
                        }
                         print!("{}",last_res);
                         return -1;
                    }
                    if buf.contains(">") {
                        // loop{
                        let mut request_mb:json_struct::RequestModbusConfInfo = json_struct::RequestModbusConfInfo::new();
                        request_mb.set_dev_id(VALUE);
                        request_mb.set_sta_id(0);
                        let _res = self.fd.write(&(request_mb.struct_to_json().as_bytes()));
                        thread::sleep(Duration::from_secs(2));    
                        let mut tcp_response: [u8; 300] = [0; 300];
                        let _res = self.fd.read(&mut tcp_response);
                        match _res {
                            Ok(ok) => {
                                    let temp_string = String::from_raw_parts(tcp_response.as_mut_ptr(), ok, ok);
                                    print!("{}\n", temp_string);
                                    return 0;
                            },
                        Err(err) => {
                            print!("respond read fails, err : {:?}\n", err);
                            thread::sleep(time::Duration::from_millis(100));
                            count = count + 1;
                            if count >= 10 {
                                print!(" module may have problems");
                                return -1;
                            }
                            }
                        }
                    }
                    if buf.contains("OK"){
                        thread::sleep(Duration::from_millis(100));
                        mem::forget(buf);
                        print!("\n");
                        return 0;
                    }
                    return -1;
                },
                Err(err) => {
                    count = count + 1;
                    if count >= 5 {
                        println!("check_read exec failed, err = {}", err);
                    }
                }
            
                

            }
        }   
    }
    pub fn gprs_init(&mut self){ 
        println!("Module connectivity check");
        self.check_write(b"AT\r\n");                    //检查是否连通
        thread::sleep(Duration::from_millis(100));
        let ret = self.check_read();
        if ret < 0{
            return;
        }
        
        self.check_write(b"ATE\r\n");                    //ATE0关闭回显，ATE打开回显
        thread::sleep(Duration::from_millis(100));
        self.check_read();

        println!("check sim ready");
        self.check_write(b"AT+CPIN?\r\n");               //check sim or ready
        thread::sleep(Duration::from_millis(100));
        self.check_read();
       
        println!("signal quality");
        self.check_write(b"AT+CSQ\r\n");                  //query signal quality
        thread::sleep(Duration::from_millis(100)); 
        self.check_read();
       
        println!("registration network");
        self.check_write(b"AT+CREG?\r\n");                 //network registration
        thread::sleep(Duration::from_millis(100));
        self.check_read();
        
        println!("ip head");
        self.check_write(b"AT+CIPHEAD=0\r\n");                 //不要IP头  +IPD50
        thread::sleep(Duration::from_millis(100));
        self.check_read();
        // self.check_write(b"AT+CGCLASS=B\r\n");         //gprs mode station class
        // thread::sleep(Duration::from_secs(2));
        println!("attached gprs"); 
        self.check_write(b"AT+CGATT=1\r\n");             //gprs packet domain attach
        thread::sleep(Duration::from_millis(100));     
        self.check_read();
        
        println!("enable gprs"); 
        self.check_write(b"AT+CGACT=1,1\r\n");           //enable gprs         
        thread::sleep(Duration::from_millis(100));   
        self.check_read();
      
        // //开机第一次时不启动，后面每次都启动一次   
        // self.check_write(b"AT+NETCLOSE\r\n");          //close packet network
        // thread::sleep(Duration::from_secs(1));
        // self.check_read();
        // println!("close network");

        println!("open network");
        self.check_write(b"AT+NETOPEN\r\n");            //open packet network
        thread::sleep(Duration::from_millis(100));
        self.check_read();
           
    }
    pub fn tcp_connect(&mut self,server_ip:&str,server_port:u32){
        println!("establish tcp connection");
        let buf = format!("AT+CIPOPEN=1,\"TCP\",\"{}\",{}\r\n",
                          server_ip,
                          server_port);
        self.check_write(&(buf.into_bytes()));
        thread::sleep(Duration::from_secs(2));
        self.check_read();
        loop{
            let buf2 = format!("AT+CIPSEND=1,52\r\n");
            self.check_write(&(buf2.into_bytes()));
            thread::sleep(Duration::from_secs(2));
            self.check_read();
            thread::sleep(Duration::from_secs(10));
        }
    }

}



