use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::time;
use std::thread;
pub struct ModBus {
    fd: File,
}
impl ModBus {
    pub fn new(path: &str) -> Self {
        let _fd = fs::File::open(path);
        let f = _fd.unwrap();
        return ModBus { fd: f };
    }
    //Datalen应该是指字节.CRC校验函数
    fn crc16(&mut self, pushmsg: &[u8], datalen: u8) -> u16 {
        //let mut i = 0;
        //let mut j = 0;
        let mut crc_sumx = 0xffff;
        // let mut c = 0;
        for i in 0..datalen {
            let c = (pushmsg[i as usize] & 0x00ff) as u16;
            crc_sumx ^= c;
            for _ in 0..8 {
                if (crc_sumx & 0x0001) > 0 {
                    crc_sumx >>= 1;
                    crc_sumx ^= 0xA001;
                } else {
                    crc_sumx >>= 1;
                }
            }
        }
        return crc_sumx;
    }
    pub fn modbus_write(&mut self, buf: &[u8]) -> usize {
        let rec = self.fd.write(buf).unwrap();
        return rec;
    }
    pub fn modbus_read(&mut self, buf: &mut [u8]) -> usize {
        let rec = self.fd.read(buf).unwrap();
        return rec;
    }
    // //功能码01;
    // pub fn read_coll_state(
    //     slave_addr: u8,
    //     reg_start_addr: u16,
    //     data_len: u16,
    //     path: &str,
    //     opt: u8,
    // ) {
    //     let func_code: u8 = 01;
    //     let mut input_state_num: u16 = 0;
    //     let reg_start_addr_high: u8 = ((reg_start_addr & 0xff00) >> 8) as u8;
    //     let reg_start_addr_low: u8 = (reg_start_addr & 0xff) as u8;
    //     let data_len_high: u8 = ((data_len & 0xff00) >> 8) as u8;
    //     let data_len_low: u8 = (data_len & 0xff) as u8;
    //     let send_data: [u8; 6] = [
    //         slave_addr,
    //         func_code,
    //         reg_start_addr_high,
    //         reg_start_addr_low,
    //         data_len_high,
    //         data_len_low,
    //     ];
    //     let crc16_high: u8 = ((ModBus::crc16(&send_data, 6) & 0xff00) >> 8) as u8;
    //     let crc16_low: u8 = (ModBus::crc16(&send_data, 6) & 0xff) as u8;
    //     let send_data_crc: [u8; 8] = [
    //         slave_addr,
    //         func_code,
    //         reg_start_addr_high,
    //         reg_start_addr_low,
    //         data_len_high,
    //         data_len_low,
    //         crc16_high,
    //         crc16_low,
    //     ];
    //     let bit_num_remainder: u16 = data_len % 8;
    //     let bit_num_rounding: u16 = data_len / 8;
    //     if 0 == bit_num_remainder {
    //         input_state_num = bit_num_rounding;
    //     } else {
    //         input_state_num = bit_num_rounding + 1;
    //     }
    //     let mut rec_buf = vec![0; (5 + (input_state_num as u8)) as usize];
    //     let mut rec_buf_return = vec![0; (13 + (input_state_num as u8)) as usize];
    //     let mut opt_file = ModBus::new(path);
    //     let _send_write = opt_file.modbus_write(&send_data_crc);
    //     if _send_write >= 0 {
    //         print!("write success\n");
    //     } else {
    //         print!("write error\n");
    //     }
    //     thread::sleep(time::Duration::from_millis((20 * data_len) as u64));
    //     if 1 == opt {
    //         let _rec_read = opt_file.modbus_read(&mut rec_buf);
    //         if _rec_read > 0 {
    //             print!("read success\n");
    //         } else {
    //             print!("read error\n");
    //         }
    //     // return rec_buf;
    //     } else if 2 == opt {
    //         let _rec_read = opt_file.modbus_read(&mut rec_buf_return);
    //         if _rec_read > 0 {
    //             print!("read success\n");
    //         } else {
    //             print!("read error\n");
    //         }
    //         //return rec_buf_return;
    //     }
    // }


    // //功能码02;
    // pub fn read_input_state(
    //     slave_addr: u8,
    //     reg_start_addr: u16,
    //     data_len: u16,
    //     path: &str,
    //     opt: u8,
    // ) {
    //     let func_code: u8 = 02;
    //     let mut input_state_num: u16 = 0;
    //     let reg_start_addr_high: u8 = ((reg_start_addr & 0xff00) >> 8) as u8;
    //     let reg_start_addr_low: u8 = (reg_start_addr & 0xff) as u8;
    //     let data_len_high: u8 = ((data_len & 0xff00) >> 8) as u8;
    //     let data_len_low: u8 = (data_len & 0xff) as u8;
    //     let send_data: [u8; 6] = [
    //         slave_addr,
    //         func_code,
    //         reg_start_addr_high,
    //         reg_start_addr_low,
    //         data_len_high,
    //         data_len_low,
    //     ];
    //     let crc16_high: u8 = ((ModBus::crc16(&send_data, 6) & 0xff00) >> 8) as u8;
    //     let crc16_low: u8 = (ModBus::crc16(&send_data, 6) & 0xff) as u8;
    //     let send_data_crc: [u8; 8] = [
    //         slave_addr,
    //         func_code,
    //         reg_start_addr_high,
    //         reg_start_addr_low,
    //         data_len_high,
    //         data_len_low,
    //         crc16_high,
    //         crc16_low,
    //     ];
    //     let bit_num_remainder: u16 = data_len % 8;
    //     let bit_num_rounding: u16 = data_len / 8;
    //     if 0 == bit_num_remainder {
    //         input_state_num = bit_num_rounding;
    //     } else {
    //         input_state_num = bit_num_rounding + 1;
    //     }
    //     let mut rec_buf = vec![0; (5 + (input_state_num as u8)) as usize];
    //     let mut rec_buf_return = vec![0; (13 + (input_state_num as u8)) as usize];
    //     let mut opt_file = ModBus::new(path);
    //     let _send_write = opt_file.modbus_write(&send_data_crc);
    //     if _send_write >= 0 {
    //         print!("write success\n");
    //     } else {
    //         print!("write error\n");
    //     }
    //     thread::sleep(time::Duration::from_millis((20 * data_len) as u64));
    //     if 1 == opt {
    //         let _rec_read = opt_file.modbus_read(&mut rec_buf);
    //         if _rec_read > 0 {
    //             print!("read success\n");
    //         } else {
    //             print!("read error\n");
    //         }
    //     // return rec_buf;
    //     } else if 2 == opt {
    //         let _rec_read = opt_file.modbus_read(&mut rec_buf_return);
    //         if _rec_read > 0 {
    //             print!("read success\n");
    //         } else {
    //             print!("read error\n");
    //         }
    //         //return rec_buf_return;
    //     }
    // }


    //功能码03;RTU模式
    //slave_addr:表示从机地址, reg_start_addr:表示要读取寄存器的起始地址,也就是要读取的第一个寄存器的地址, reg_data_num:表示要读取几个寄存器(寄存器个数)
    //read_mult_reg_input:表示功能码03,读取多路寄存器的输入值.opt为1时表示返回值只是从机返回的值,opt为2时，表示返回值
    //包括主机发送+从机返回的。
    pub fn read_holding_reg(
        &mut self,
        slave_addr: u8,            //mb_addr
        reg_start_addr: u16,        //start_addr
        reg_data_num: u16,          //seg_num
        buf: &mut [u8],          //length
        opt: u8) -> i16 {           //设为1
        
        let func_code: u8 = 03;
        let mut send_data: [u8; 8] = [
            slave_addr,
            func_code,
            ((reg_start_addr & 0xff00) >> 8) as u8,
            (reg_start_addr & 0xff) as u8,
            ((reg_data_num & 0xff00) >> 8) as u8,
            (reg_data_num & 0xff) as u8,
            0,
            0,
        ];
        send_data[6] = ((self.crc16(&send_data, 6) & 0xff00) >> 8) as u8;
        send_data[7] = (self.crc16(&send_data, 6) & 0xff) as u8;

        let mut rec_buf = vec![0; (5 + ((reg_data_num * 2) as u8)) as usize];
        let mut rec_buf_return = vec![0; (13 + ((reg_data_num * 2) as u8)) as usize];
        let _send_write = self.modbus_write(&send_data);
        if _send_write > 0 {
            print!("write success\n");
            thread::sleep(time::Duration::from_millis(
                (13 + (reg_data_num * 2)) as u64,
            ));
            if 1 == opt {
                let _rec_read = self.modbus_read(&mut rec_buf);
                if _rec_read > 0 {
                    print!("read success\n");
                    let mut rec_buf_part = vec![0; (3 + ((reg_data_num * 2) as u8)) as usize];
                    for i in 0..((3 + ((reg_data_num * 2) as u8)) as usize) {
                        rec_buf_part[i] = rec_buf[i];
                    }
                    let crc_rec_part_high: u8 =
                        (((self.crc16(&rec_buf_part, (3 + ((reg_data_num * 2) as u8)))) &
                              0xff00) >> 8) as u8;
                    let crc_rec_part_low: u8 =
                        (((self.crc16(&rec_buf_part, (3 + ((reg_data_num * 2) as u8)))) &
                            0xff)) as u8;
                    if (crc_rec_part_high == rec_buf[(3 + ((reg_data_num * 2) as u8)) as usize]) &&
                        (crc_rec_part_low == rec_buf[(4 + ((reg_data_num * 2) as u8)) as usize])
                    {
                        for i in 0..((reg_data_num * 2) as u8) as usize {
                            buf[i] = rec_buf[3 + i];
                            println!("{:?}",buf[i]);
                        }
                        return (reg_data_num * 2) as i16;
                    } else {
                        return -1;
                    }
                } else {
                    print!("read error\n");
                    return -1;

                }
            } else if 2 == opt {
                let _rec_read = self.modbus_read(&mut rec_buf_return);
                if _rec_read > 0 {
                    print!("read success\n");
                    let mut rec_buf_return_part =
                        vec![0; (3 + ((reg_data_num * 2) as u8)) as usize];
                    for i in 0..((3 + ((reg_data_num * 2) as u8)) as usize) {
                        rec_buf_return_part[i] = rec_buf_return[8 + i];
                    }
                    let crc_rec_part_high: u8 = ((self.crc16(
                        &rec_buf_return_part,
                        (3 + ((reg_data_num * 2) as u8)),
                    ) & 0xff00) >> 8) as u8;
                    let crc_rec_part_low: u8 =
                        ((self.crc16(&rec_buf_return_part, (3 + ((reg_data_num * 2) as u8))) &
                              0xff)) as u8;
                    if (crc_rec_part_high ==
                            rec_buf_return[(11 + ((reg_data_num * 2) as u8)) as usize]) &&
                        (crc_rec_part_low ==
                             rec_buf_return[(12 + ((reg_data_num * 2) as u8)) as usize])
                    {
                        for i in 0..((reg_data_num * 2) as u8) as usize {
                            buf[i] = rec_buf_return[11 + i];
                        }
                        return (reg_data_num * 2) as i16;
                    } else {
                        return -1;
                    }
                } else {
                    print!("read error\n");
                    return -1;
                }
            } else {
                return -2;
            }
        } else {
            print!("write error\n");
            return -1;
        }
    }EXIT






    //功能码16
    //接收到的buf中包含的返回值为起始地址和写入寄存器的个数，都是2字节
    pub fn wirte_mult_coll(
        &mut self,
        slave_addr: u8,
        reg_start_addr: u16,
        write_reg_num: u16,
        write_reg_content: &mut [u16],
        buf: &mut [u8],
        opt: u8,
    ) -> i16 {
        let func_code: u8 = 16;
        let mut send_data = vec![0; (9 + 2 * write_reg_num) as usize];
        send_data[0] = slave_addr;
        send_data[1] = func_code;
        send_data[2] = ((reg_start_addr & 0xff00) >> 8) as u8;
        send_data[3] = (reg_start_addr & 0xff) as u8;
        send_data[4] = ((write_reg_num & 0xff00) >> 8) as u8;
        send_data[5] = (write_reg_num & 0xff) as u8;
        for i in 6..(9 + 2 * write_reg_num) as usize {
            send_data[i] = 0;
        }
        for i in 0..(write_reg_num) {
            send_data[(6 + i) as usize] = ((write_reg_content[i as usize] & 0xff00) >> 8) as u8;
            send_data[(7 + i) as usize] = (write_reg_content[i as usize] & 0xff) as u8;
        }
        send_data[(7 + 2 * write_reg_num) as usize] =
            (((self.crc16(&send_data, ((7 + 2 * write_reg_num) as u8))) & 0xff00) >> 8) as u8;
        send_data[(8 + 2 * write_reg_num) as usize] =
            ((self.crc16(&send_data, ((7 + 2 * write_reg_num) as u8))) & 0xff) as u8;
        let mut rec_buf: [u8; 8] = [0; 8];
        let mut rec_buf_return = vec![0; (17 + ((write_reg_num * 2) as u8)) as usize];
        let _send_write = self.modbus_write(&send_data);
        if _send_write > 0 {
            print!("write success\n");
            thread::sleep(time::Duration::from_millis(
                (17 + (write_reg_num * 2)) as u64,
            ));
            if 1 == opt {
                let _rec_read = self.modbus_read(&mut rec_buf);
                if _rec_read > 0 {
                    print!("read success\n");
                    let mut rec_buf_part: [u8; 6] = [0; 6];
                    for i in 0..6 {
                        rec_buf_part[i] = rec_buf[i];
                    }
                    let crc_rec_part_high: u8 = (((self.crc16(&rec_buf_part, 6)) & 0xff00) >>
                        8) as u8;
                    let crc_rec_part_low: u8 = (((self.crc16(&rec_buf_part, 6)) & 0xff)) as u8;
                    if (crc_rec_part_high == rec_buf[6]) && (crc_rec_part_low == rec_buf[7]) {
                        for i in 0..4 {
                            buf[i] = rec_buf[2 + i];
                        }
                        return 4;
                    } else {
                        print!("read return CRC error\n");
                        return -2;
                    }
                } else {
                    print!("read error\n");
                    return -1;
                }
            } else if 2 == opt {
                let _rec_read = self.modbus_read(&mut rec_buf_return);
                if _rec_read > 0 {
                    let mut rec_buf_return_part: [u8; 6] = [0; 6];
                    for i in 0..4 {
                        rec_buf_return_part[i] =
                            rec_buf_return[(i + 9 + ((write_reg_num * 2) as usize)) as usize]
                    }
                    let crc_rec_part_high: u8 =
                        (((self.crc16(&rec_buf_return_part, 6)) & 0xff00) >> 8) as u8;
                    let crc_rec_part_low: u8 = (((self.crc16(&rec_buf_return_part, 6)) & 0xff)) as
                        u8;
                    if (crc_rec_part_high ==
                            rec_buf_return_part[(15 + ((write_reg_num * 2) as u8)) as usize]) &&
                        (crc_rec_part_low ==
                             rec_buf_return_part[(16 + ((write_reg_num * 2) as u8)) as usize])
                    {
                        for i in 0..4 {
                            buf[i] = rec_buf[(i + 11 + ((write_reg_num * 2) as usize)) as usize];
                        }
                        return 4;
                    } else {
                        print!("read return CRC error\n");
                        return -2;
                    }
                } else {
                    print!("read error\n");
                    return -1;
                }
            } else {
                return -1;
            }
        } else {
            print!("write error\n");
            return -1;
        }
    }

    // //功能码04
    // pub fn read_input_reg(
    //     slave_addr: u8,
    //     reg_start_addr: u16,
    //     reg_data_num: u16,
    //     path: &str,
    //     opt: u8,
    // ) {
    //     let func_code: u8 = 04;
    //     let reg_start_addr_high: u8 = ((reg_start_addr & 0xff00) >> 8) as u8;
    //     let reg_start_addr_low: u8 = (reg_start_addr & 0xff) as u8;
    //     let data_len_high: u8 = ((reg_data_num & 0xff00) >> 8) as u8;
    //     let data_len_low: u8 = (reg_data_num & 0xff) as u8;
    //     let send_data: [u8; 6] = [
    //         slave_addr,
    //         func_code,
    //         reg_start_addr_high,
    //         reg_start_addr_low,
    //         data_len_high,
    //         data_len_low,
    //     ];
    //     let crc16_high: u8 = ((ModBus::crc16(&send_data, 6) & 0xff00) >> 8) as u8;
    //     let crc16_low: u8 = (ModBus::crc16(&send_data, 6) & 0xff) as u8;
    //     let send_data_crc: [u8; 8] = [
    //         slave_addr,
    //         func_code,
    //         reg_start_addr_high,
    //         reg_start_addr_low,
    //         data_len_high,
    //         data_len_low,
    //         crc16_high,
    //         crc16_low,
    //     ];
    //     let mut rec_buf = vec![0; (5 + ((reg_data_num * 2) as u8)) as usize];
    //     let mut rec_buf_return = vec![0; (13 + ((reg_data_num * 2) as u8)) as usize];
    //     let mut opt_file = ModBus::new(path);
    //     let _send_write = opt_file.modbus_write(&send_data_crc);
    //     if _send_write >= 0 {
    //         print!("write success\n");
    //     } else {
    //         print!("write error\n");
    //     }
    //     thread::sleep(time::Duration::from_millis((20 * reg_data_num) as u64));
    //     if 1 == opt {
    //         let _rec_read = opt_file.modbus_read(&mut rec_buf);
    //         if _rec_read > 0 {
    //             print!("read success\n");
    //         } else {
    //             print!("read error\n");
    //         }
    //     //return rec_buf;
    //     } else if 2 == opt {
    //         let _rec_read = opt_file.modbus_read(&mut rec_buf_return);
    //         if _rec_read > 0 {
    //             print!("read success\n");
    //         } else {
    //             print!("read error\n");
    //         }
    //         //return rec_buf_return;
    //     }
    // }
}